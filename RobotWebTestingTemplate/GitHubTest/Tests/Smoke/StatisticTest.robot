*** Settings ***
Library  StepsLibrary
Library  Selenium2Library

*** Test Cases ***
Test Case 001
    Go To  https://github.com/robotframework/robotframework
    On Project Page Click Language Usage Diagram
    On Project Page Log Language Usage Statistics
    On Project Page Verify That Language Usage Adds Up To 100
