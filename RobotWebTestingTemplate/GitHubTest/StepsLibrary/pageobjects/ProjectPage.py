from ..helpers.Element import Element


class ProjectPage:
    def __init__(self):
        pass

    @staticmethod
    def get_lang_stats_graph_locator():
        return "xpath=//div[@title='Click for language details']"

    @staticmethod
    def get_lang_list_locator():
        return "css=ol.repository-lang-stats-numbers"

    @staticmethod
    def get_lang_names_locator():
        return ProjectPage.get_lang_list_locator() + " span.lang"

    @staticmethod
    def get_lang_percents_locator():
        return ProjectPage.get_lang_list_locator() + " span.percent"

    @staticmethod
    def click_lang_stats_graph():
        Element.wait_and_click_element(
            ProjectPage.get_lang_stats_graph_locator())

    @staticmethod
    def get_lang_names_list():
        lib = Element.get_selenium2_library()
        lib.wait_until_element_is_visible(
            ProjectPage.get_lang_names_locator())
        spans = lib.get_webelements(
            ProjectPage.get_lang_names_locator())
        return [span.text for span in spans]

    @staticmethod
    def get_lang_percents_list():
        lib = Element.get_selenium2_library()
        lib.wait_until_element_is_visible(
            ProjectPage.get_lang_percents_locator())
        spans = lib.get_webelements(
            ProjectPage.get_lang_percents_locator())
        return [span.text for span in spans]
