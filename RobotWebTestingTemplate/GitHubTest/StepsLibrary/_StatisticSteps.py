from pageobjects.ProjectPage import ProjectPage


class _StatisticSteps(object):
    def __init__(self):
        pass

    def on_project_page_click_language_usage_diagram(self):
        print "Clicking language usage graph"
        ProjectPage.click_lang_stats_graph()

    def on_project_page_log_language_usage_statistics(self):
        print "Statistics for language usage in current project:"
        lang_names = ProjectPage.get_lang_names_list()
        lang_share = ProjectPage.get_lang_percents_list()
        for name, share in zip(lang_names, lang_share):
            print name, "-", share

    def on_project_page_verify_that_language_usage_adds_up_to_100(self):
        lang_share = ProjectPage.get_lang_percents_list()
        total = sum([eval(number_string.replace('%', ''))
                     for number_string in lang_share])
        print "Total language usage:", total
        if abs(total - 100.0) > 0.1:
            raise ValueError(
                "Total language usage should be equal to 100%")
