*** Settings ***
Resource  ../../PageObjects/HomePage.robot
Resource  ../../PageObjects/ArticlePage.robot
Library  Selenium2Library
Force Tags  search

*** Test Cases ***
Search By Language
    [Tags]  TC001
    Go To Home Page
    At Home Page Type Search Expression  Roma
    At Home Page Select Language  Latina
    At Home Page Start Search
    On Article Page Article Title Should Be  Roma
    Location Should Contain  https://la.wikipedia.org
