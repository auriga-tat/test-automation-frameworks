*** Settings ***
Documentation  A page object describing elements of the site home page
Library  Selenium2Library
Resource  Utils/Element.robot


*** Variables ***
${SEARCH_LANGUAGE_LOCATOR}=  id=searchLanguage
${SEARCH_TEXT_LOCATOR}=  id=searchInput
${SEARCH_BUTTON_LOCATOR}=  xpath=//button[@name='go']


*** Keywords ***
Go To Home Page
    Go To  https://www.wikipedia.org

At Home Page Select Language
    [Arguments]  ${language}
    Log  Selecting language ${language}
    Wait And Select List Item  ${SEARCH_LANGUAGE_LOCATOR}  ${language}

At Home Page Type Search Expression
    [Arguments]  ${text}
    Log  Typing search text ${text}
    Wait And Enter Text  ${SEARCH_TEXT_LOCATOR}  ${text}

At Home Page Start Search
    Log  Pressing Search Button
    Wait And Click Element  ${SEARCH_BUTTON_LOCATOR}
