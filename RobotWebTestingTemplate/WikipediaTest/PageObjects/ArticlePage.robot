*** Settings ***
Documentation  A page object describing elements of the article page
Resource  Utils/Element.robot

*** Variables ***
${ARTICLE_HEADER_LOCATOR}=  id=firstHeading

*** Keywords ***
On Article Page Article Title Should Be
    [Arguments]  ${expected_title}
    Log  Checking Article title
    Log  Expected title: ${expected_title}
    ${actual_title}=  Wait And Get Visible Text  ${ARTICLE_HEADER_LOCATOR}
    Should Be Equal  ${actual_title}  ${expected_title}
