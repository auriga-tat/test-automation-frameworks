*** Settings ***
Documentation  This file includes standard preconditions and post-conditions
...            that may be used for test suites and individual tests.
...            - In a test suite you may include them in Settings:
...            "Suite Precondition" and "Suite Postcondition"
...            (or "Suite Setup" and "Suite Teardown")
...            - To include to all individual tests within a suite or a file, use in Settings:
...            "Test Precondition" and "Test Postcondition"
...            (or "Test Setup" and "Test Teardown")
...            - Within a test case use:
...            [Precondition] and [Postconditon]
...            (or [Setup] and [Teardown])
Library  Selenium2Library
Resource  Settings.robot


*** Keywords ***
Standard Setup Procedure
    # Opens a browser and URL specified in Settings.robot
    Open Browser  ${HOST_URL}  ${BROWSER}
    # Sets a default timeout to wait for an element to appear
    Set Selenium Timeout  ${SELENIUM_TIMEOUT}
    Maximize Browser Window
    Log  Initialized successfully

Standard Teardown Procedure
    Close All Browsers
