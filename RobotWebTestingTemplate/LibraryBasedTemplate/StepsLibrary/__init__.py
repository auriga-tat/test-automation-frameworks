from _HomePageSteps import _HomePageSteps


class StepsLibrary(
    # include all classes with custom coded keywords to import them all at once
    _HomePageSteps
):
    """
    This class should be imported to scripts as:

    *** Settings ***
    Library  StepsLibrary

    """

    ROBOT_LIBRARY_SCOPE = 'GLOBAL'

    def __init__(self):
        pass
