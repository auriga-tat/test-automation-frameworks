from helpers.Element import Element
from pageobjects.HomePage import HomePage


class _HomePageSteps(object):
    """
    Steps for the Home Page
    """

    def __init__(self):
        pass

    # Add Home Page-related keywords here
    #Example:
    def on_home_page_do_something(self):
        print "Hello"
