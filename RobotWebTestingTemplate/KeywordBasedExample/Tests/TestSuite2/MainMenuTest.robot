*** Settings ***
Documentation  Example test of the site navigation menu in a parameterized (data-driven) form
Resource            ../../PageObjects/MainMenu.robot
Library  Selenium2Library
Force Tags          main-menu


*** Test Cases ***

Main Menu Navigation
    [Tags]  TC002
    # This is an example of a parameterized data-driven test
    # After [Template] we put name of the keyword with the test body
    [Template]  Template Test - Main Menu Navigation
    # After [Template], we only put the table of arguments passed to the test in each iteration
    # ${main_menu_item}  ${sub_menu_item}  ${expected_page_title}
    Компания    Офисы Ауриги                Офисы Ауриги
    КОМПАНИЯ    Контакты                    Contact Us
    Экспертиза  Автоматизация тестирования  Test Automation


*** Keywords ***

# In local Keywords, we put the body of the parameterized test
Template Test - Main Menu Navigation
    # We specify parameters that the test will accept in each iteration
    [Arguments]  ${main_menu_item}  ${sub_menu_item}  ${expected_page_title}
    # So, we simulate puting the mouse cursor over the main menu, so that submenu items appear
    Mouse Over Main Menu Item  ${main_menu_item}
    # Now we click an item with a given name (2nd parameter)
    Click Main Menu Sub-Item  ${sub_menu_item}
    # Validate the page title when it opens
    Title Should Be  ${expected_page_title}

