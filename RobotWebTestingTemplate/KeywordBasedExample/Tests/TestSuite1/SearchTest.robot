*** Settings ***
Documentation  Example test for searching on the page
Resource            ../../PageObjects/SiteHeader.robot
Resource            ../../PageObjects/SearchResultsPage.robot
Library  Selenium2Library
Force Tags          search


*** Test Cases ***

Search by single word
    [Tags]  TC001
    # Using keyword imported from page object: ../../PageObjects/SiteHeader.robot
    # Enters text to a search field in header and initiates search
    Search On Site For Text  Selenium
    # Test results should contain a link to the page "Test Automation"
    # Using keyword imported from page object: ../../PageObjects/SearchResultsPage.robot
    Verify A Search Result Link Is Visible  Test Automation
    # Using another keyword imported from SearchResultsPage.robot to navigate to the page
    Click A Search Result Link  Test Automation
    # Validate the page title when it opens
    Title Should Be  Test Automation
