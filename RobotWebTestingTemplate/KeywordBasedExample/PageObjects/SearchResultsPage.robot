*** Settings ***
Documentation  A page object describing a pane with search results
Library  Selenium2Library
Resource  Utils/Element.robot


*** Variables ***
${SEARCH_PANE_LOCATOR}=  xpath=//div[@class='search-page']


*** Keywords ***
Get A Search Result Link With Text
    [Documentation]  Accepts 1 argument - the text of the link in search results.
    ...              Returns a WebElement object referring to that link if it exists, else fails.
    [Arguments]  ${link_text}
    ${link}=  Wait For Element  ${SEARCH_PANE_LOCATOR}//a[text()='${link_text}']
    [return]  ${link}

Verify A Search Result Link Is Visible
    [Documentation]  Accepts 1 argument - the text of the link in search results.
    ...              Verifies that the link is visible, else fails.
    [Arguments]  ${link_text}
    ${link}=  Get A Search Result Link With Text  ${link_text}
    ${visibility}=  Get Visibility Of Element  ${link}
    Should Be True  ${visibility}

Click A Search Result Link
    [Documentation]  Clicks a link with given text if it is present in search results, else fails.
    [Arguments]  ${link_text}
    ${link}=  Get A Search Result Link With Text  ${link_text}
    Click Element  ${link}

