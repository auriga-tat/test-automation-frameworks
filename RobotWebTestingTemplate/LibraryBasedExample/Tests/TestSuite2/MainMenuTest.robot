*** Settings ***
Documentation  Example test of the site navigation menu in a parameterized (data-driven) form
Library  Selenium2Library
Library  StepsLibrary
Force Tags  main-menu


*** Test Cases ***

Main Menu Navigation
    [Tags]  TC002
    # This is an example of a parameterized data-driven test
    # After [Template] we put name of the keyword with the test body
    [Template]  Template Test - Main Menu Navigation
    # After [Template], we only put the table of arguments passed to the test in each iteration
    # ${main_menu_item}  ${sub_menu_item}  ${expected_page_title}
    Компания    Офисы Ауриги                Офисы Ауриги
    КОМПАНИЯ    Контакты                    Contact Us
    Экспертиза  Автоматизация тестирования  Test Automation


*** Keywords ***

# In local Keywords, we put the body of the parameterized test
Template Test - Main Menu Navigation
    # We specify parameters that the test will accept in each iteration
    [Arguments]  ${main_menu_item}  ${sub_menu_item}  ${expected_page_title}
    When I navigate in the Main Menu through items  ${main_menu_item}  ${sub_menu_item}
    Then I should see page title  ${expected_page_title}

