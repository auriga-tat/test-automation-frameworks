from helpers.Element import Element
from pageobjects.MainMenu import MainMenu


class _GeneralSteps(object):
    """
    Steps for the general site layout and properties
    """

    def __init__(self):
        pass

    def i_should_see_page_title(self, expected_title):
        lib = Element.get_selenium2_library()
        lib.title_should_be(expected_title)

    def i_navigate_in_the_main_menu_through_items(self, item_text, subitem_text):
        print "Opening menu: " + item_text
        MainMenu.mouse_over_main_menu_item(item_text)
        print "Choosing menu item: " + subitem_text
        MainMenu.click_main_menu_subitem(subitem_text)
