from selenium.webdriver.common.keys import Keys

from ..helpers.Element import Element


class SiteHeader:
    """
    Page Object giving access to elements of site header (except the navigation menu)
    """

    def __init__(self):
        pass

    @staticmethod
    def get_search_field():
        """
        Locates the search field of the site header.
        :return: A WebElement object corresponding to the search field.
        """
        return Element.wait_for_element('css=form.search input')

    @staticmethod
    def search_on_site_for_text(search_text):
        """
        Enters text to the search field and initiates the search.
        :param search_text: the text that will be entered to search field
        """
        search_field = SiteHeader.get_search_field()
        search_field.clear()
        search_field.click()
        search_field.send_keys(search_text + Keys.ENTER)
