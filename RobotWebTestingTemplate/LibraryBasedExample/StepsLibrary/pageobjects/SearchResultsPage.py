from ..helpers.Element import Element


class SearchResultsPage:
    """
    Page Object to access elements in the search results pane
    """

    def __init__(self):
        pass

    @staticmethod
    def get_search_pane_locator():
        return "xpath=//div[@class='search-page']"

    @staticmethod
    def get_search_link_by_text_locator(link_text):
        """
        Generates locator for a link in search results, containing exact text.
        :param link_text: The text of the link
        :return: XPATH locator of the link
        """
        return (SearchResultsPage.get_search_pane_locator() +
                "//a[text()='" + link_text + "']")

    @staticmethod
    def get_search_link_by_text(link_text):
        """
        Locates a link in search results, containing exact text.
        :param link_text: The text of the link
        :return: The WebElement instance of the link if it exists, None if it does not.
        """
        locator = SearchResultsPage.get_search_link_by_text_locator(link_text)
        return Element.wait_for_element(locator)

    @staticmethod
    def get_search_link_visibility_by_text(link_text):
        """
        Locates a link in search results, containing exact text, and checks its visibility.
        :param link_text: The text of the link
        :return: True or False for visibility if the link exists, False if it does not.
        """
        locator = SearchResultsPage.get_search_link_by_text_locator(link_text)
        return Element.get_visibility_of_element(locator)

    @staticmethod
    def click_search_link_by_text(link_text):
        locator = SearchResultsPage.get_search_link_by_text_locator(link_text)
        return Element.wait_and_click_element(locator)
