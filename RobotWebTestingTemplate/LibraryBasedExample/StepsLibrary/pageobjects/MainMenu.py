from ..helpers.Element import Element


class MainMenu:
    """
    Page Object for working with the site's main navigation menu
    """

    def __init__(self):
        pass

    @staticmethod
    def get_main_menu_locator():
        return "xpath=//ul[@id='nav']"

    @staticmethod
    def get_main_menu_item_locator(item_text):
        """
        Gets XPATH to locate a top-level main menu item by text.
        :param item_text: The displayed text
        :return: locator suitable for finding the element
        """
        return (MainMenu.get_main_menu_locator() +
                "/li/a/span[text()='" + item_text + "']")

    @staticmethod
    def get_main_menu_subitem_locator(subitem_text):
        """
        Gets XPATH to locate a second-level main menu item by text.
        :param subitem_text: The displayed text
        :return: locator suitable for finding the element
        """
        return (MainMenu.get_main_menu_locator() +
                "//ul[@class='sub-list']/li/a[text()='" + subitem_text + "']")

    @staticmethod
    def mouse_over_main_menu_item(item_text):
        """
        Simulates moving mouse over the required top-level menu item.
        :param item_text: The displayed text
        """
        locator = MainMenu.get_main_menu_item_locator(item_text)
        Element.mouse_over(locator)

    @staticmethod
    def click_main_menu_subitem(subitem_text):
        """
        Simulates a mouse click on the required second-level menu item.
        The menu should already be displayed.
        :param subitem_text: The displayed text
        """
        locator = MainMenu.get_main_menu_subitem_locator(subitem_text)
        Element.wait_and_click_element(locator)
