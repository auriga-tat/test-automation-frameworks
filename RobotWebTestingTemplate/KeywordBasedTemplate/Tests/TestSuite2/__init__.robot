*** Settings ***
Documentation  This is the __init__ file for the 2nd test suite
...            The suite contains some demo tests.
Resource  ../SetupAndTeardown.robot
# Standard procedures are taken from resource SetupAndTeardown.robot
# But you may define any specific procedures for a particular suite
Suite Precondition  Standard Setup Procedure
Suite Teardown  Standard Teardown Procedure
