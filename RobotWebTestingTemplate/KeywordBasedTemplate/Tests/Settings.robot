*** Variables ***
# Basic url that is used to open the tested application
${HOST_URL}=  http://www.mydomain.org

# Add other settings common for your domain here
# For example:
# ${USER_LOGIN}=  robotester
# ${USER_PASSWORD}=  Test123

# Available browser names are:
# firefox, ff
# chrome, googlechrome, gc
# internetexplorer, ie
# phantonjs
# htmlunit, htmlunitwithjs
${BROWSER}=  chrome

# Define standard timeout to wait until an element appears at the page.
# If you make it too short, then clicking or verifying an element that has
# not yet appeared will make a test fail.
${SELENIUM_TIMEOUT}=  20 seconds

# If you need to start the browser with specific capabilities, this parameter
# should contain a string in format "key1:val1,key2:val2,...".
# For specific capabilities refer to documentation of a driver.
# E.g., for capturing JavaScript logs in Chrome, set:
# ${DESIRED_CAPABILITIES}=  webdriver.log.driver:ALL
${DESIRED_CAPABILITIES}=  None

