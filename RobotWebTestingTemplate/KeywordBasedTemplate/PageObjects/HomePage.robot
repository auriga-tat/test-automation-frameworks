*** Settings ***
Documentation  A page object describing elements of the site home page
Library  Selenium2Library
Resource  Utils/Element.robot


*** Variables ***
${LOGIN_LINK_LOCATOR}=  xpath=//a[text()='Sign in']


*** Keywords ***

Click Login Link
    [Documentation]  Clicks the Login link on the home page
    Wait And Click Element  ${LOGIN_LINK_LOCATOR}

