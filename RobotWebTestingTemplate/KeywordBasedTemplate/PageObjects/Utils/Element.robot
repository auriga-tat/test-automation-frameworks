*** Settings ***
Library  Selenium2Library
Documentation  This file is used to add user-defined keywords
...            to manipulate elements on pages.
...            It is used to add methods that are not present in the standard library.
...            In case we repeatedly use the same sequence of keywords to work with elements,
...            it is better to include this sequence to a keyword resource file.


*** Keywords ***
Wait For Element
    [Arguments]  ${locator}
    [Documentation]  Use this keyword to get an instance of WebElement object
    ...              and remember it in a variable.
    ...              This keyword first waits for the element to appear
    ...              but fails if timeout is longer than usual.
    Wait Until Page Contains Element  ${locator}
    ${element}=  Get Webelement  ${locator}
    [return]  ${element}

Wait And Click Element
    [Arguments]  ${locator}
    [Documentation]  Use this keyword to click an element that may appear
    ...              on the screen only after sime timeout.
    ...              It fails if timeout is longer than usual.
    Wait Until Element Is Visible  ${locator}
    Click Element  ${locator}

Get Attribute Of Element
    [Arguments]  ${locator}  ${attribute}
    [Documentation]  Use this keyword to return a value of some attribute
    ...              of an html tag e.g. class or placeholder.
    ${element}=  Wait For Element  ${locator}
    # Keyword "Get Element Attribute" requires a specific format,
    # so it's easier here to call the method directly
    ${attribute_value}=  Call Method  ${element}  get_attribute  ${attribute}
    [return]  ${attribute_value}

Get Visibility Of Element
    [Arguments]  ${locator}
    [Documentation]  This method returns ${true} if element is currently visible,
    ...              ${false} if invisible, but fails if there is no such element.
    ${element}=  Wait For Element  ${locator}
    ${visibility}=  Call Method  ${element}  is_displayed
    [return]  ${visibility}

