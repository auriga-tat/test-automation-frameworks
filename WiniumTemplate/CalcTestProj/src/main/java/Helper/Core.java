package Helper;

import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.net.URL;


public class Core {
    public WebDriver driver;
    public WebDriverWait wait;

    @Before
    public void setUp() throws Exception {
        // Do not forget to run Winium.Desktop.Driver.exe before!
        DesiredCapabilities cap = new DesiredCapabilities();
        cap.setCapability("app","C:\\Windows\\system32\\calc.exe");
        this.driver = new RemoteWebDriver(new URL("http://localhost:9999"),cap);
        this.wait = new WebDriverWait(driver, 30);
    }

    @After
    public void tearDown() throws Exception {
    }
}
