package Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by arkadiy.hachikyan on 5/13/2016.
 */
public class ButtonsArea {

    public WebDriver driver;
    public WebDriverWait wait;
    public WebElement window;

    public ButtonsArea(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        this.window = driver.findElement(By.className("CalcFrame"));
    }


    public void clickButton2() {driver.findElement(By.xpath("/*[contains(@AutomationId,'158')]"));}

    public void clickButtonPlus() {
        window.findElement(By.id("93")).click();//+
    }

    public void clickButton4() {
        window.findElement(By.id("134")).click();//4
    }

    public void clickButtonEqual() {
        window.findElement(By.id("121")).click();//=
    }

}
