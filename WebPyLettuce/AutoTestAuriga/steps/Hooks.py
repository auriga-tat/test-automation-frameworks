# -*- coding: utf-8 -*-

"""
In this file, "before" and "after" hooks are defined for tests, steps, feature, etc.
(The code in the template is rather self-explanatory, so each hook is not commented below)
"""

from lettuce import before, after

from AutoTestAuriga.helper import core
from AutoTestAuriga.pages.Common import Common


@before.each_scenario
def before_scenario(scenario):
    core.init_driver()
    Common().open_main_page()


@after.each_step
def after_step(step):
    if step.failed:
        file_prefix = core.make_valid_for_filename(step.scenario.name)
        core.save_screenshot(file_prefix)


@after.each_scenario
def after_scenario(scenario):
    core.close_driver()
