# -*- coding: utf-8 -*-

"""
Module contains a group of steps related to basic navigation in the Wiki.
"""

from lettuce import step

from AutoTestAuriga.pages.TopPanel import TopPanel
from AutoTestAuriga.pages.WikiPage import WikiPage


@step(r'I see that page header is "(.*)"')
def i_see_that_page_header_is(step, expected_header):
    WikiPage().verify_page_header(expected_header)


@step(r'on Top Panel I enter a search phrase "(.*)"')
def on_top_panel_i_enter_a_search_phrase(step, search_phrase):
    TopPanel().search_for_text(search_phrase)
