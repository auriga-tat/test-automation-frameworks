# -*- coding: utf-8 -*-

"""
Module contains a group of steps related to working with Index page.
"""

from lettuce import step

from AutoTestAuriga.pages.IndexPage import IndexPage
from AutoTestAuriga.pages.LeftPanel import LeftPanel


@step('on Left Panel I click the Index link')
def on_left_panel_i_click_the_index_link(step):
    LeftPanel().click_index_link()


@step('on Index Page I see following navigation links:')
def on_left_panel_i_click_the_index_link(step):
    for link in step.hashes:
        IndexPage().get_navigation_element_by_text(link["text"])
