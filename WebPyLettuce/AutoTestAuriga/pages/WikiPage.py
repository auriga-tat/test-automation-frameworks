# -*- coding: utf-8 -*-

from nose.tools import assert_equal
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from AutoTestAuriga.pages.Common import Common


class WikiPage(Common):
    """
    This page object contains methods that are common for any Wikipedia page.
    """

    def get_header_element(self):
        return self.wait.until(EC.presence_of_element_located((By.XPATH, u"//h1[@id='firstHeading']")))

    def get_header_element_by_text(self, expected_title_text):
        return self.wait.until(EC.presence_of_element_located((
            By.XPATH,
            u"//h1[@id='firstHeading' and text()='" + expected_title_text + u"']"
        )))

    def verify_page_header(self, expected_title_text):
        try:
            self.get_header_element_by_text(expected_title_text)
        except:
            actual_title_text = self.get_header_element().text
            assert_equal(actual_title_text, expected_title_text, "Page header is incorrect")
