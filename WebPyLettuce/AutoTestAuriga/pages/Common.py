# -*- coding: utf-8 -*-

from AutoTestAuriga.helper import core

MAIN_PAGE_URL = "http://ru.wikipedia.org"


class Common(object):
    """
    A common parent for all PageObjects
    It is a template, so Common page should not contain any application-specific locators.
    """
    def __init__(self):
        self.driver = core.get_current_driver()
        self.wait = core.get_driver_wait()
        self.logger = core.get_logger(__file__)

    def open_main_page(self):
        self.logger.info("Opening Main page...")
        self.driver.get(MAIN_PAGE_URL)
