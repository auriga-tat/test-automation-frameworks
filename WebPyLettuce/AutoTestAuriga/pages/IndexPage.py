# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from AutoTestAuriga.pages.Common import Common


class IndexPage(Common):
    """
    This page object contains methods that interact with the elements of Index page.
    """

    def get_navigation_element_by_text(self, text_of_navigation_link):
        return self.wait.until(EC.presence_of_element_located((
            By.XPATH,
            u"//a[text()='" + text_of_navigation_link + u"']"
        )))
