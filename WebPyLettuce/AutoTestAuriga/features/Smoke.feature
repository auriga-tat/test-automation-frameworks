Feature: Smoke tests

  @TC001
  Scenario: View index page contents
    When on Left Panel I click the Index link
    Then I see that page header is "Википедия:Алфавитный указатель"
    And on Index Page I see following navigation links:
      | text  |
      | А     |
      | БА—БЯ |
      | Ва    |

  @TC002
  Scenario Outline: Search for existing articles
    When on Top Panel I enter a search phrase "<search phrase>"
    Then I see that page header is "<expected article>"

    Examples:
      | search phrase                         | expected article                      |
      | Тестирование программного обеспечения | Тестирование программного обеспечения |
      | Питон                                 | Питоны                                |