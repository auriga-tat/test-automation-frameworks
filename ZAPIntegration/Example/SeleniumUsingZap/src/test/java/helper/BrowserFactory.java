package helper;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.Proxy;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.IOException;

/**
 * A class that allows to create different types of browser sessions
 */
public class BrowserFactory {

    public static final Logger logger = LogManager.getLogger(BrowserFactory.class);

    /**
     * Returns a new WebDriver instance for a requested type
     *
     * @param driverName Type of browser connection: Firefox, Chrome, IE or Android
     * @throws IOException
     * @throws RuntimeException
     */
    public static WebDriver getNewWebDriver(String driverName) throws IOException, RuntimeException {
        driverName = driverName.toUpperCase();
        if (driverName.equals("FF") || driverName.equals("FIREFOX")) {
            return getNewFirefox();
        } else if (driverName.equals("CHROME")) {
            return getNewChrome();
        } else if (driverName.equals("IE") || driverName.equals("IEXPLORER") || driverName.equals("INTERNETEXPLORER")) {
            return getNewInternetExplorer();
        } else {
            throw new IllegalArgumentException("Browser type not supported: " + driverName);
        }
    }

    public static Proxy getProxy() {
        String proxyHost = System.getProperty("proxyHost");
        String proxyPort = System.getProperty("proxyPort");
        logger.info("Selenium will work via proxy: " + proxyHost + ":" + proxyPort);
        if (proxyHost == null || proxyPort == null) return null;
        else return new Proxy()
                .setSslProxy(proxyHost + ":" + proxyPort)
                .setHttpProxy(proxyHost + ":" + proxyPort);
    }

    public static DesiredCapabilities getCommonDesiredCapabilities() {
        DesiredCapabilities result = new DesiredCapabilities();
        Proxy proxy = getProxy();
        if (proxy != null) {
            result.setCapability(CapabilityType.PROXY, proxy);
        }
        return result;
    }

    /**
     * Returns a Firefox session
     *
     * @throws IOException
     */
    public static FirefoxDriver getNewFirefox() throws IOException {
        DesiredCapabilities capabilities = getCommonDesiredCapabilities();
        return new FirefoxDriver(capabilities);
    }

    /**
     * Returns a Chrome or Chromium session
     */
    public static ChromeDriver getNewChrome() {
        DesiredCapabilities capabilities = getCommonDesiredCapabilities();
        return new ChromeDriver(capabilities);
    }

    /**
     * Returns an Internet Explorer session
     *
     * @throws RuntimeException
     */
    public static InternetExplorerDriver getNewInternetExplorer() throws RuntimeException {
        DesiredCapabilities capabilities = getCommonDesiredCapabilities();
        return new InternetExplorerDriver(capabilities);
    }

}
