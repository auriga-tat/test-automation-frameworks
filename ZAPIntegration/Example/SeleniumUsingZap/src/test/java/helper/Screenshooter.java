package helper;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Screenshooter {
    private static final Logger logger = LogManager.getLogger(Screenshooter.class);

    /**
     * Set the default path to save screenshots to
     */
    private static final String SCREENSHOTS_FOLDER = "C:\\home\\screenshots";

    /**
     * Set the default filename format
     */
    private static final DateFormat FILE_NAME_FORMAT =
            new SimpleDateFormat("yyyy-MM-dd_HH-mm-ss.SSS");

    /**
     * This method takes care of copying a temporary file to a defined place and creates directory structure if necessary
     *
     * @param temporaryFile A File object to be copied
     * @param storageFolder An absolute path of a storage folder
     * @param fileName      The name of the target file
     * @return Absolute path to the file stored
     */
    private static String storeFile(File temporaryFile, String storageFolder, String fileName) throws RuntimeException {
        // Create folders if not exist
        Path path = FileSystems.getDefault().getPath(storageFolder);
        try {
            if (Files.notExists(path)) {
                Files.createDirectories(path);
            }
        } catch (IOException e) {
            logger.error("Folder creation failed for path: " + storageFolder, e);
            throw new RuntimeException("Failed to access screenshots folder");
        }

        // Store the file to the appropriate folder with given name
        String filePath = FileSystems.getDefault().getPath(storageFolder, fileName).toString();
        try {
            FileUtils.copyFile(temporaryFile, new File(filePath));
        } catch (IOException e) {
            logger.error("Exception while saving a screenshot", e);
            filePath = "File was not saved!";
        }
        return filePath;
    }

    /**
     * Takes screenshot of an active browser content.
     * Switches to native context if we are testing a mobile web view.
     *
     * @return Path to the new image file
     */
    public static String takeScreenshot(WebDriver driver) {
        try {

            File screenshotTempFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

            return storeFile(screenshotTempFile, SCREENSHOTS_FOLDER, FILE_NAME_FORMAT.format(new Date()) + ".png");

        } catch (Exception e) {
            return "Failed to take screenshot (Exception: " + e.getClass() + "; Message: " + e.getMessage() + ")";
        }
    }
}
