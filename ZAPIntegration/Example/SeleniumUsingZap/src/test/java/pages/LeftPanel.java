package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


/**
 * A page object that describes behavior of the left panel drawer
 */
public class LeftPanel extends Common {
    public LeftPanel(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    /**
     * Searches for a link in side menu containing specific text
     */
    private WebElement getLinkByText(String text) {
        try {
            return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
                    "//div[@class='menu view-border-box']//li/a[text()='" + text + "']")));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Gets the transparent shield element that covers other content when menu is open.
     * While this element is not a part of the side menu, it is included here
     * since it will only be used related to the menu.
     */
    private WebElement getTransparentShield() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.transparent-shield")));
    }

    private void waitMenuBeingClosed() {
        wait.until(ExpectedConditions.invisibilityOfElementLocated(By.cssSelector("div.transparent-shield")));
    }

    /**
     * Clicks the transparent shield so the menu would be closed
     */
    public void closeLeftPanel() {
        WebElement shield = getTransparentShield();
        if (shield.isDisplayed()) {
            logger.info("Clicking the opaque shield to close the left panel");
            shield.click();
            waitMenuBeingClosed();
        } else {
            logger.info("Left panel does not seem to be open");
        }
    }

    /**
     * Fails if a menu item with the expected text does not exist or is invisible
     */
    public void verifyLinkIsVisible(String text) {
        logger.info("Searching for a link in the left panel: " + text);
        WebElement menuLink = getLinkByText(text);
        org.junit.Assert.assertTrue("Link should be present: " + text,
                menuLink != null && menuLink.isDisplayed());
    }

}
