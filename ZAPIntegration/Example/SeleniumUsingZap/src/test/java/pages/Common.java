package pages;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * A page object to perform common actions.
 * Common means not related to specific page and could be performed on any page.
 * It is also a parent to all page objects,
 * so it contains inheritable fields for WebDriver, Wait and Logger
 */
public class Common {
    public static final Logger logger = LogManager.getLogger(Common.class);
    public final WebDriver driver;
    public final WebDriverWait wait;

    public Common(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    /**
     * Get the header element for the current page or article
     */
    private WebElement getHeaderElement() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.heading-holder h1")));
    }

    /**
     * Goes to the initial page
     */
    public void openMainPage() {
        logger.info("Opening Main page...");
        driver.get("https://en.m.wikipedia.org");
    }

    /**
     * Fails if current page header is not equal to expected
     */
    public void verifyPageHeader(String expectedPageTitle) {
        String currentPageHeader = getHeaderElement().getText();
        org.junit.Assert.assertEquals("Page header is incorrect", expectedPageTitle, currentPageHeader);
    }
}
