package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * A page object which describes controls and actions related only to Top panel
 */
public class TopPanel extends Common {
    public TopPanel(WebDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    /**
     * Searches for the button that opens the side menu
     */
    private WebElement getMenuButton() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(
                By.cssSelector("a.main-menu-button")));
    }

    /**
     * Searches for the input field that is displayed when it is not focused.
     * It is really a fake field, you can not input anything to it.
     */
    private WebElement getFakeSearchInput() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//div[@class='header']//input[@name='search']")));
    }

    /**
     * Searches for the real search input that appears after the fake input field is clicked
     */
    private WebElement getDynamicSearchInput() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(
                By.xpath("//div[@class='overlay-header']//input[@name='search']")));
    }

    /**
     * Clicks the menu button to open the left menu panel
     */
    public void clickMainMenuButton() {
        getMenuButton().click();
    }

    /**
     * Enters the search text and initiates search
     */
    public void searchForText(String textForSearching) {
        logger.info("Searching for text = " + textForSearching);
        getFakeSearchInput().click();
        getDynamicSearchInput().clear();
        getDynamicSearchInput().sendKeys(textForSearching + Keys.ENTER);
    }

}
