------------------------
ZAP Maven Plugin notice
(C) 2015, Auriga, Inc.
------------------------
This product is distibuted under Apache license v.2.0.
You must keep current file according to the par. 4(d) of the license (see LICENSE.txt).

Originally developed by ZAP community since 2012.
Discontinued in 2013, see https://code.google.com/archive/p/zap-maven-plugin/source/default/commits

version 1.2 - Last free version available at Google Code

version 1.3 - Modified by Auriga, Inc. to work with ZAP ClientApi v.2.4
              Incompatible with v.1.2 (List of configuration parameters has substantially changed)
