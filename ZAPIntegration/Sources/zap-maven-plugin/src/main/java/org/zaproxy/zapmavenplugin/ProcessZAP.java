package org.zaproxy.zapmavenplugin;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.zaproxy.clientapi.core.ApiResponse;
import org.zaproxy.clientapi.core.ApiResponseElement;
import org.zaproxy.clientapi.core.ClientApi;
import org.zaproxy.clientapi.core.ClientApiException;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

/**
 * Goal for running scans in ZAP and gathering reports.
 */
@Mojo(name = "process-zap",
        defaultPhase = LifecyclePhase.POST_INTEGRATION_TEST,
        threadSafe = true,
        requiresDependencyResolution = ResolutionScope.TEST
)
public class ProcessZAP extends AbstractMojo {

    private ClientApi zapClientAPI;
    private Proxy proxy;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");

    /**
     * Location of the host of the ZAProxy
     */
    @Parameter(defaultValue = "localhost", property = "zap.zapProxyHost", required = true)
    private String zapProxyHost;

    /**
     * Location of the port of the ZAProxy
     */
    @Parameter(defaultValue = "8080", property = "zap.zapProxyPort", required = true)
    private int zapProxyPort;

    /**
     * API key (in ZAP, located in Options -> API)
     */
    @Parameter(property = "zap.zapApiKey", required = true)
    private String zapApiKey;

    /**
     * Root URL of the web application for attack
     */
    @Parameter(property = "zap.targetURL", required = true)
    private String targetURL;

    /**
     * Switch to spider the URL
     */
    @Parameter(property = "zap.doSpiderScan", defaultValue = "true")
    private boolean doSpiderScan;

    /**
     * Maximum timeout for the Spider task in seconds
     */
    @Parameter(defaultValue = "1200", property = "zap.spiderTimeout")
    private int spiderTimeout;

    /**
     * Switch to scan the URL
     */
    @Parameter(defaultValue = "true", property = "zap.doActiveScan")
    private boolean doActiveScan;

    /**
     * Maximum timeout for the Active Scan task in seconds
     */
    @Parameter(defaultValue = "1200", property = "zap.scanTimeout")
    private int scanTimeout;

    /**
     * Save session snapshot after scan (snapshot will be stored in ZAP user sessions folder)
     */
    @Parameter(defaultValue = "true", property = "zap.saveSession")
    private boolean saveSession;

    /**
     * Switch to shutdown ZAP after completion
     */
    @Parameter(defaultValue = "true", property = "zap.shutdownZAP")
    private boolean shutdownZAP;

    /**
     * Save report about security alerts
     */
    @Parameter(defaultValue = "true", property = "zap.reportAlerts")
    private boolean reportAlerts;

    /**
     * Location to store the ZAP reports
     */
    @Parameter(defaultValue = "${project.build.directory}/zap-reports", property = "zap.reportsDirectory")
    private String reportsDirectory;

    /**
     * Report output format type, may be one of: xml, json, html, fine.html
     */
    @Parameter(defaultValue = "fine.html", property = "zap.reportFormat")
    private String reportFormat;

    /**
     * create a temporary filename
     *
     * @param prefix if null, then default "temp"
     * @param suffix if null, then default ".tmp"
     */
    private String createTempFilename(String prefix, String suffix) {
        return new StringBuilder()
                .append(prefix != null ? prefix : "temp")
                .append(dateFormat.format(new Date()))
                .append("_")
                .append(UUID.randomUUID().toString())
                .append(suffix != null ? suffix : ".tmp")
                .toString();
    }

    /**
     * Change the ZAP API status response to an integer
     *
     * @param response the ZAP API response code
     */
    private int statusToInt(ApiResponse response) {
        return Integer.parseInt(((ApiResponseElement) response).getValue());
    }

    /**
     * Search for all links and pages on the URL
     *
     * @param url the to investigate URL
     * @throws ClientApiException
     */
    private void spiderURL(String url) throws ClientApiException {
        zapClientAPI.spider.scan(zapApiKey, url, null, "true", null);

        long breakTime = System.currentTimeMillis()
                + spiderTimeout * 1000;

        int currentProgress;
        while ((currentProgress = statusToInt(zapClientAPI.spider.status(null))) < 100) {

            if (System.currentTimeMillis() >= breakTime) {
                getLog().warn("Spider progress: [" + currentProgress + "%]. Breaking on timeout");
                zapClientAPI.spider.stopAllScans(zapApiKey);
                break;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                getLog().error(e);
            }
        }
    }

    /**
     * Scan all pages found at url
     *
     * @param url the url to scan
     * @throws ClientApiException
     */
    private void scanURL(String url) throws ClientApiException {
        zapClientAPI.ascan.scan(zapApiKey, url, "true", "false", "", "", "");

        long breakTime = System.currentTimeMillis()
                + scanTimeout * 1000;

        int currentProgress;
        while ((currentProgress = statusToInt(zapClientAPI.ascan.status(null))) < 100) {

            if (System.currentTimeMillis() >= breakTime) {
                getLog().warn("Active scan progress: [" + currentProgress + "%]. Breaking on timeout");
                zapClientAPI.ascan.stopAllScans(zapApiKey);
                break;
            }

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                getLog().error(e);
            }
        }
    }

    /**
     * Get a format for retrieving alerts
     *
     * @param format is one of: fine.html, html, xml, json; defaults to xml
     */
    private String getReportFormat(String format) {

        if (format.equalsIgnoreCase("xml") ||
                format.equalsIgnoreCase("html") ||
                format.equalsIgnoreCase("fine.html") ||
                format.equalsIgnoreCase("json")) {
            return format;
        } else {
            return "xml";
        }

    }

    /**
     * Get all alerts from ZAP proxy
     *
     * @param format is one of: fine.html, html, xml, json
     * @return all alerts from ZAProxy
     * @throws Exception
     */
    private String getAllAlerts(String format) throws Exception {
        String result = "";

        if (!format.equalsIgnoreCase("fine.html")) {

            URL url = new URL("http://zap/" + format + "/core/view/alerts");

            getLog().info("Open URL: " + url.toString());

            HttpURLConnection uc = (HttpURLConnection) url.openConnection(proxy);
            uc.connect();

            BufferedReader in = new BufferedReader(new InputStreamReader(
                    uc.getInputStream()));
            String inputLine;

            while ((inputLine = in.readLine()) != null) {
                result = result + inputLine;
            }

            in.close();
        } else {
            // fine.html
            getLog().info("Retrieving a formatted HTML report from ZAP...");

            byte[] content = zapClientAPI.core.htmlreport(zapApiKey);
            result = new String(content);
        }
        return result;

    }

    /**
     * Execute the goal
     *
     * @throws MojoExecutionException
     */
    public void execute() throws MojoExecutionException {

        List<Throwable> exceptionCollector = new LinkedList<>();
        try {

            zapClientAPI = new ClientApi(zapProxyHost, zapProxyPort);
            proxy = new Proxy(Proxy.Type.HTTP, new InetSocketAddress(zapProxyHost, zapProxyPort));

            if (doSpiderScan) {
                getLog().info("Spider the site [" + targetURL + "]");
                try {
                    spiderURL(targetURL);
                } catch (Throwable t) {
                    getLog().error("Spider task ended with exception");
                    exceptionCollector.add(t);
                }
            } else {
                getLog().info("Skip spidering the site [" + targetURL + "]");
            }

            if (doActiveScan) {
                getLog().info("Scan the site [" + targetURL + "]");
                try {
                    scanURL(targetURL);
                } catch (Throwable t) {
                    getLog().error("Scan task ended with exception");
                    exceptionCollector.add(t);
                }
            } else {
                getLog().info("Skip scanning the site [" + targetURL + "]");
            }

            if (saveSession) {
                getLog().info("Saving the session snapshot approximately at ["
                        + dateFormat.format(new Date()) + "]; look for the file in ZAP user sessions folder");
                try {
                    zapClientAPI.core.snapshotSession(zapApiKey);
                } catch (Throwable t) {
                    getLog().error("Session snapshot saving exception");
                    exceptionCollector.add(t);
                }
            } else {
                getLog().info("Skip saving session to file");
            }

            if (reportAlerts) {
                try {
                    File directory = new File(reportsDirectory);
                    if (!directory.exists()) {
                        getLog().info("Creating path: [" + reportsDirectory + "]");
                        directory.mkdirs();
                    }

                    String format = getReportFormat(reportFormat);
                    String fileName = createTempFilename("ZAP", "");
                    String fullFileName = FilenameUtils.concat(reportsDirectory, fileName)
                            + "." + format;

                    String alerts = getAllAlerts(format);

                    getLog().info("Saving alerts in format [" + format + "]");
                    FileUtils.writeStringToFile(new File(fullFileName), alerts);
                    getLog().info("Saved alerts to file [" + fullFileName + "]");
                } catch (Throwable t) {
                    getLog().error("Failed to save alerts to file in [" + reportsDirectory + "]");
                    exceptionCollector.add(t);
                }
            }

        } catch (Throwable t) {
            getLog().error(t);
            throw new MojoExecutionException("Processing with ZAP failed", t);
        } finally {
            // Log all stack traces
            int numExceptions = exceptionCollector.size();
            if (numExceptions > 0) {
                getLog().error("" + numExceptions + " exceptions occured during ZAP Processing:");
                for (Throwable t : exceptionCollector) {
                    getLog().error(t);
                }
            }

            // Shutdown ZAP if required
            if (shutdownZAP && (zapClientAPI != null)) {
                try {
                    getLog().info("Shutdown ZAProxy");
                    zapClientAPI.core.shutdown(zapApiKey);
                } catch (Exception e) {
                    getLog().error(e);
                }
            } else {
                getLog().info("No shutdown of ZAP");
            }
        }
    }

}
