package org.zaproxy.zapmavenplugin;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.zaproxy.clientapi.core.ClientApi;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;


/**
 * Goal which will start ZAP proxy.
 */
@Mojo(name = "start-zap",
        defaultPhase = LifecyclePhase.PRE_INTEGRATION_TEST,
        threadSafe = true,
        requiresDependencyResolution = ResolutionScope.TEST
)
public class StartZAP extends AbstractMojo {
    /**
     * Location of the ZAProxy program. Required if newProcess=true.
     */
    @Parameter(property = "zap.zapProgram")
    private String zapProgram;

    /**
     * Location of the host of the ZAProxy
     */
    @Parameter(defaultValue = "localhost", property = "zap.zapProxyHost", required = true)
    private String zapProxyHost;

    /**
     * Location of the port of the ZAProxy
     */
    @Parameter(defaultValue = "8080", property = "zap.zapProxyPort", required = true)
    private int zapProxyPort;

    /**
     * API key (in ZAP, located in Options -> API). Required if newProcess=false.
     */
    @Parameter(property = "zap.zapApiKey")
    private String zapApiKey;

    /**
     * Given session name for the session file in ZAP directory
     */
    @Parameter(defaultValue = "Maven", property = "zap.sessionName", required = true)
    private String sessionName;

    /**
     * Start ZAProxy process automatically; if false, ZAP should be running before executing this goal
     */
    @Parameter(defaultValue = "false", property = "zap.newProcess")
    private boolean newProcess;

    /**
     * Sleep to wait for ZAProxy to fully start (ms)
     */
    @Parameter(defaultValue = "20000", property = "zap.waitBeforeStart")
    private int waitBeforeStart;

    /**
     * Execute the goal
     *
     * @throws MojoExecutionException
     */
    public void execute()
            throws MojoExecutionException {
        try {
            if (!newProcess) {
                if (zapApiKey == null) {
                    getLog().error("Parameter 'zapApiKey' is required to create a new session for a running ZAP. " +
                            "If you just want to start a new ZAP instance, set parameter 'newProcess'=true");
                    throw new MojoExecutionException("Invalid parameter set");
                }

                ClientApi zapClient = new ClientApi(zapProxyHost, zapProxyPort);
                getLog().info("Create Session with mame [" + sessionName + "]");
                zapClient.core.newSession(zapApiKey, sessionName, "true");
            } else {
                if (zapProgram == null) {
                    getLog().error("Parameter 'zapProgram' if 'newProcess'=true");
                    throw new MojoExecutionException("Invalid parameter set");
                }

                File pf = new File(zapProgram);
                Runtime runtime = java.lang.Runtime.getRuntime();
                getLog().info("Start ZAProxy [" + zapProgram + "]");
                getLog().info("Using working directory [" + pf.getParentFile().getPath() + "]");
                final Process ps = runtime.exec(zapProgram, null, pf.getParentFile());

                // Consommation de la sortie standard de l'application externe dans un Thread separe
                new Thread() {
                    public void run() {
                        try {
                            BufferedReader reader = new BufferedReader(new InputStreamReader(ps.getInputStream()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    // Traitement du flux de sortie de l'application si besoin est
                                    getLog().info(line);
                                }
                            } finally {
                                reader.close();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();

                // Consommation de la sortie d'erreur de l'application externe dans un Thread separe
                new Thread() {
                    public void run() {
                        try {
                            BufferedReader reader = new BufferedReader(new InputStreamReader(ps.getErrorStream()));
                            String line;
                            try {
                                while ((line = reader.readLine()) != null) {
                                    // Traitement du flux d'erreur de l'application si besoin est
                                    getLog().info(line);
                                }
                            } finally {
                                reader.close();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                }.start();

            }

            Thread.sleep(waitBeforeStart);
        } catch (Exception e) {
            getLog().error(e);
            throw new MojoExecutionException("Unable to start ZAP [" + zapProgram + "]", e);
        }

    }
}
