package pages;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * A page object to perform common actions for any activity screen.
 * It is also a parent to all page objects,
 * so it contains inheritable fields for WebDriver, Wait and Logger
 */
public class Common {
    protected static final Logger logger = LogManager.getLogger(Common.class);
    protected final AppiumDriver driver;
    protected final WebDriverWait wait;

    public Common(AppiumDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    /**
     * Gets the id of the current activity (screen) in Android
     */
    public String getCurrentActivity() {
        if (driver instanceof AndroidDriver) {
            return ((AndroidDriver) driver).currentActivity();
        } else {
            return "";
        }
    }

    /**
     * Searches for a TextView element with given text
     */
    protected WebElement getTextLabel(String text) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
                "//android.widget.TextView[@text='" + text + "']")));
    }

    /**
     * Clicks a TextView element with given text
     */
    public void clickTextLabel(String text) {
        logger.info("Clicking on text: " + text);
        getTextLabel(text).click();
    }

    /**
     * Fails if there is no TextView element with given text or it is not visible
     */
    public void verifyTextLabelIsVisible(String text) {
        logger.info("Searching for text: " + text);
        try {
            WebElement label = getTextLabel(text);
            org.junit.Assert.assertTrue("Text should be visible: " + text, label.isDisplayed());
        } catch (Exception e) {
            logger.error("Text was not found", e);
            org.junit.Assert.fail("Text should be present: " + text);
        }
    }

}
