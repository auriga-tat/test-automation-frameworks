package pages;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * A page object to describe screen for adding a blood pressure measurement
 */
public class BloodPressureEdit extends Common {

    public BloodPressureEdit(AppiumDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    private WebElement getSystolicField() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
                "//*[@resource-id='com.michaelfester.heart.lite:id/sys']/android.widget.EditText")));
    }

    private WebElement getDiastolicField() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
                "//*[@resource-id='com.michaelfester.heart.lite:id/dia']/android.widget.EditText")));
    }

    private WebElement getRateField() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
                "//*[@resource-id='com.michaelfester.heart.lite:id/rate']/android.widget.EditText")));
    }

    private WebElement getAddReadingButton() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//android.widget.Button[@text='Add reading']")));
    }

    private WebElement getLastValueLabel() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(
                "//*[@resource-id='com.michaelfester.heart.lite:id/lastValue']")));
    }

    public void enterSystolicPressure(String value) {
        logger.info("Putting value " + value + " to Systolic pressure field");
        WebElement field = getSystolicField();
        field.click();
        field.clear();
        field.sendKeys(value);
    }

    public void enterDiastolicPressure(String value) {
        logger.info("Putting value " + value + " to Diastolic pressure field");
        WebElement field = getDiastolicField();
        field.click();
        field.clear();
        field.sendKeys(value);
    }

    public void enterHeartRate(String value) {
        logger.info("Putting value " + value + " to Heart Rate field");
        WebElement field = getRateField();
        field.click();
        field.clear();
        field.sendKeys(value);
    }

    public void clickAddReadingButton() {
        logger.info("Clicking a footer button to add reading");
        getAddReadingButton().click();
    }

    /**
     * When a BP Edit screen is reopened, the last entered value is shown.
     * This method gets the displayed value in the same format as it is displayed ("sys | dia | pulse").
     */
    public String getLastValue() {
        try {
            String result = getLastValueLabel().getText();
            logger.info("Last value displayed: " + result);
            return result;
        } catch (Exception e) {
            logger.info("Last entered value not found");
            return "";
        }
    }

}
