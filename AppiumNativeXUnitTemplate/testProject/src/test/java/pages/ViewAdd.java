package pages;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * A page object to describe screen displayed after pressing "Add" at the Home screen
 */
public class ViewAdd extends Common {
    public ViewAdd(AppiumDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    /*
    * In example tests there are no specific actions for this page.
    * We only need text labels that are supported in Common class.
    */

}
