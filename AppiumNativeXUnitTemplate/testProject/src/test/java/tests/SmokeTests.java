package tests;

import helper.BaseTestClass;
import org.junit.Ignore;
import org.junit.Test;
import pages.BloodPressureEdit;
import pages.Home;
import pages.ViewAdd;

// Class which contains set of smoke tests. It is just logical separation.
// It is meaningful to group tests to classes by their goals or business logic.
public class SmokeTests extends BaseTestClass {

    // @Test is standard JUnit notation to mark methods which perform tests
    @Test
    public void verifyHomePageActionsVisibility() throws Exception {
        Home homeScreen = new Home(driver, wait);
        homeScreen.verifyActionIconWithLabelIsVisible("Add");
        homeScreen.verifyActionIconWithLabelIsVisible("Journals");
        homeScreen.verifyActionIconWithLabelIsVisible("Stats");
        homeScreen.verifyActionIconWithLabelIsVisible("Send");
        homeScreen.verifyActionIconWithLabelIsVisible("Prefs");
        homeScreen.verifyActionIconWithLabelIsVisible("About");
    }

    @Test
    public void verifyLastBPValueInBPEditor() throws Exception {
        // Go to Add screen
        new Home(driver, wait).pressActionIconWithLabel("Add");

        // Enter Blood Pressure editor
        ViewAdd addScreen = new ViewAdd(driver, wait);
        addScreen.clickTextLabel("BP reading");

        // Enter and save value; app automatically returns to Add screen
        BloodPressureEdit bpEditScreen = new BloodPressureEdit(driver, wait);
        bpEditScreen.enterSystolicPressure("130");
        bpEditScreen.enterDiastolicPressure("95");
        bpEditScreen.enterHeartRate("45");
        bpEditScreen.clickAddReadingButton();

        // Reopen Blood Pressure editor and verify the last value
        addScreen.clickTextLabel("BP reading");
        org.junit.Assert.assertEquals("Last value should be", "130 | 95 | 45", bpEditScreen.getLastValue());
    }

    //This method will be ignored and excluded from testing. To make this test runnable just remove @Ignore notation
    @Ignore
    @Test
    public void thisTestFails() throws Exception {
        // Going to Journals screen and trying to click something that is not there
        new Home(driver, wait).pressActionIconWithLabel("Journals");
        new ViewAdd(driver, wait).clickTextLabel("BP reading");
    }

}
