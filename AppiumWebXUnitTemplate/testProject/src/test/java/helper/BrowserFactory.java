package helper;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * A class that allows to create different types of browser sessions
 */
public class BrowserFactory {

    public static final Logger logger = LogManager.getLogger(BrowserFactory.class);

    /**
     * Returns a new WebDriver instance for a requested type
     *
     * @param driverName Type of browser connection: Firefox, Chrome, IE or Android
     * @throws IOException
     * @throws RuntimeException
     */
    public static WebDriver getNewWebDriver(String driverName) throws IOException, RuntimeException {
        driverName = driverName.toUpperCase();
        if (driverName.equals("APPIUM") || driverName.equals("ANDROID")
                || driverName.equals("IOS") || driverName.equals("OSX")) {
            return getNewAppiumDriver();
        } else if (driverName.equals("FF") || driverName.equals("FIREFOX")) {
            return getNewFirefox();
        } else if (driverName.equals("CHROME")) {
            return getNewChrome();
        } else if (driverName.equals("IE") || driverName.equals("IEXPLORER") || driverName.equals("INTERNETEXPLORER")) {
            return getNewInternetExplorer();
        } else {
            throw new IllegalArgumentException("Browser type not supported: " + driverName);
        }
    }

    /**
     * Returns a Firefox session
     *
     * @throws IOException
     */
    public static FirefoxDriver getNewFirefox() throws IOException {
        FirefoxProfile profile = new FirefoxProfile();

        // Set preferences to not show dialog on downloading files. Download in silent mode without confirmation
//        profile.setAcceptUntrustedCertificates(true);
//        profile.setPreference("browser.helperApps.alwaysAsk.force", false);
//        profile.setPreference("browser.download.manager.showWhenStarting", false);
//        profile.setPreference("browser.download.folderList", 2);
//        profile.setPreference("browser.download.dir", "C:\\temp"); // my downloading dir
//        profile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
//        profile.setPreference("browser.download.useDownloadDir", true);
//        profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf,text/html");
//        profile.setPreference("pdfjs.disabled", true);

        // Run FireBug+FirePath if tests started in "debug" mode. Do not run FireBug+FirePath if in "run" mode.
        if (java.lang.management.ManagementFactory.getRuntimeMXBean().
                getInputArguments().toString().indexOf("-agentlib:jdwp") > 0) {
            // FireBug
            profile.addExtension(ResourceReader.getInstance()
                    .getFileFromResource("fireFoxPlugins/firebug@software.joehewitt.com.xpi"));
            profile.setPreference("extensions.firebug.currentVersion", "9.9.9");
            // FirePath
            profile.addExtension(ResourceReader.getInstance()
                    .getFileFromResource("fireFoxPlugins/FireXPath@pierre.tholence.com.xpi"));
        }
        return new FirefoxDriver(profile);
    }

    /**
     * Returns a Chrome or Chromium session
     */
    public static ChromeDriver getNewChrome() {
        System.setProperty("webdriver.chrome.driver", new File(
                "drivers/chrome/chromedriver.exe").getAbsolutePath());
        return new ChromeDriver();
    }

    /**
     * Returns an Internet Explorer session
     *
     * @throws RuntimeException
     */
    public static InternetExplorerDriver getNewInternetExplorer() throws RuntimeException {

        // You would need to set the protected mode settings to same for all zones in IE
        // See details here http://jimevansmusic.blogspot.in/2012/08/youre-doing-it-wrong-protected-mode-and.html
        String architecture = System.getProperty("os.arch");

        if (architecture.equals("x86")) {
            System.setProperty("webdriver.ie.driver", new File(
                    "src/main/resources/drivers/ie/x86/IEDriverServer.exe").getAbsolutePath());

        } else if (architecture.equals("amd64")) {
            System.setProperty("webdriver.ie.driver", new File(
                    "src/main/resources/drivers/ie/x64/IEDriverServer.exe").getAbsolutePath());

        } else {
            throw new RuntimeException("Could not properly set up the IEDriverServer for system architecture:" + architecture);
        }

        return new InternetExplorerDriver();
    }

    /**
     * A private method to collect capabilities for Appium.
     * Capabilities are listed as properties in file <i>appium.properties</i>.
     * All properties may be overridden from command line.
     */
    private static DesiredCapabilities getAppiumCapabilities() throws IOException {
        // Convert path relative to target folder to absolute
        String workingDir = BrowserFactory.class
                .getProtectionDomain().getCodeSource().getLocation().getPath();
        workingDir = StringUtils.removeStart(workingDir, "/");
        logger.info("Working directory is: " + workingDir);

        // Read file "appium.properties"
        Properties appiumProperties = new Properties();
        appiumProperties.load(new FileReader(Paths.get(workingDir, "appium.properties").toFile()));

        DesiredCapabilities capabilities = new DesiredCapabilities();
        for (Object key : appiumProperties.keySet()) {

            // Read capabilities from file, but override if equal system properties exist (with "appium." prefix)
            String capabilityName = (String) key;
            String capabilityValue = System.getProperty("appium." + capabilityName);
            if (capabilityValue == null) {
                capabilityValue = appiumProperties.getProperty(capabilityName);
            }

            if (!capabilityName.equals("app")) {
                capabilities.setCapability(capabilityName, capabilityValue);
            } else {
                // "app" is a special capability that sets a local path to an .apk
                // in properties, we set a relative path in the project resources

                capabilities.setCapability(capabilityName, Paths.get(workingDir, capabilityValue).toString());
            }
        }

        return capabilities;
    }

    /**
     * Returns a new Appium session with capabilities listed in properties
     *
     * @throws RuntimeException
     * @throws IOException
     */
    public static AppiumDriver getNewAppiumDriver() throws RuntimeException, IOException {
        // Set system properties "appium.address" and "appium.port" to specify where Appium is listening
        // The default values are 127.0.0.1:4723
        String appiumAddress = System.getProperty("appium.address");
        String appiumPort = System.getProperty("appium.port");

        String serverEndpoint = "http://" +
                (appiumAddress != null ? appiumAddress : "127.0.0.1") +
                ":" + (appiumPort != null ? appiumPort : "4723") + "/wd/hub";
        URL appiumServerUrl;
        try {
            appiumServerUrl = new URL(serverEndpoint);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("Malformed Appium server address: " + serverEndpoint);
        }

        // Return the WebDriver depending on platform type
        DesiredCapabilities capabilities = getAppiumCapabilities();
        String platform = (String) capabilities.asMap().get("platformName");
        if (platform == null) {
            throw new IllegalArgumentException("platformName property not specified!");
        } else {
            platform = platform.toUpperCase();
            if (platform.equals("ANDROID")) {
                logger.info("Creating an Android driver connected to Appium at " + serverEndpoint);
                return new AndroidDriver(appiumServerUrl, capabilities);
            } else if (platform.equals("IOS")) {
                logger.info("Creating an iOS driver connected to Appium at " + serverEndpoint);
                return new IOSDriver(appiumServerUrl, capabilities);
            } else {
                throw new IllegalArgumentException("Unsupported mobile platform: " + platform);
            }
        }

    }


}
