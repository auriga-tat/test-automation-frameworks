package helper;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * This is 3-rd party class to extract file content from resources.
 * Do not go deep into details.
 */
public class ResourceReader {
    public static final Logger logger = LogManager.getLogger(ResourceReader.class);

    private static ResourceReader instance;

    public static synchronized ResourceReader getInstance() {
        if (instance == null) {
            instance = new ResourceReader();
        }
        return instance;
    }

    public InputStream getInputStreamFromResource(String path) {
        InputStream input = null;
        try {
            input = this.getClass().getClassLoader().getResourceAsStream(path);
        } catch (Throwable t) {
            logger.error("Error getting stream from " + path, t);
        }
        return input;
    }


    public File getFileFromInputStream(InputStream in, String prefix, String suffix) throws IOException {
        final File tempFile = File.createTempFile(prefix, suffix);
        tempFile.deleteOnExit();
        FileOutputStream out = new FileOutputStream(tempFile);
        IOUtils.copy(in, out);
        return tempFile;
    }

    /**
     * @param pathToFileInResources - file path relative to resources folder.
     *                              Example: if file located in src/test/resources/drivers/chrome/chromedriver.exe put the following path:
     *                              "drivers/chrome/chromedriver.exe".
     * @return the object of File type.
     * @throws IOException
     */
    public File getFileFromResource(String pathToFileInResources) throws IOException {
        int index = pathToFileInResources.lastIndexOf("/");
        String fileName = pathToFileInResources.substring(index + 1);
        String filepath = pathToFileInResources.substring(0, index + 1);
        String basename = FilenameUtils.getBaseName(fileName);
        String extension = "." + FilenameUtils.getExtension(fileName);
        InputStream input = getInputStreamFromResource(filepath + basename + extension);
        File file = getFileFromInputStream(input, basename, extension);
        return file;
    }
}