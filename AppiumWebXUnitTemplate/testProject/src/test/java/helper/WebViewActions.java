package helper;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.awt.*;

/**
 * A class that implements specific actions for a mobile browser (or a hybrid app)
 * that require switching to native context.
 */
public class WebViewActions {
    // A tap duration in milliseconds that should typically work as a single tap. You may tune it.
    private static final int TAP_DURATION_MS = 200;

    // Typical names for mobile automation contexts
    // A more complex implementation will be needed when there are several WebViews
    public static final String NATIVE_CONTEXT_HANDLE = "NATIVE_APP";
    public static final String WEB_CONTEXT_HANDLE_PREFIX = "WEBVIEW";

    /**
     * A variable to store previous context when a context is being changed
     */
    private static String previousContext;

    public static void saveContext(AppiumDriver driver) {
        previousContext = driver.getContext();
    }

    public static void switchToWebContext(AppiumDriver driver) {
        if (driver.getContext().startsWith(WEB_CONTEXT_HANDLE_PREFIX)) return;
        saveContext(driver);
        driver.context(WEB_CONTEXT_HANDLE_PREFIX);
        (new WebDriverWait(driver, 60)).until(
                ExpectedConditions.presenceOfElementLocated(By.xpath("//body")));
    }

    public static void switchToNativeContext(AppiumDriver driver) {
        if (driver.getContext().equals(NATIVE_CONTEXT_HANDLE)) return;
        saveContext(driver);
        driver.context(NATIVE_CONTEXT_HANDLE);
        (new WebDriverWait(driver, 60)).until(
                ExpectedConditions.presenceOfElementLocated(By.xpath("//android.webkit.WebView")));
    }

    public static void restoreLastContext(AppiumDriver driver) {
        String currentContext = driver.getContext();
        if (!currentContext.equals(previousContext)) {
            if (previousContext.startsWith(WEB_CONTEXT_HANDLE_PREFIX)) {
                switchToWebContext(driver);
            } else {
                switchToNativeContext(driver);
            }
        }
    }

    /**
     * Taps the center of the element if it is visible.
     * Tap action differs from Click action, and sometimes mobile sites require tap, not click.
     * If the WebDriver provided is not an Appium driver, just clicks the element.
     */
    public static void tapOrClick(WebDriver driver, WebElement element) {
        if (!(driver instanceof AppiumDriver)) {
            element.click();
        } else {
            AppiumDriver appium = (AppiumDriver) driver;

            Rectangle webElementRect = WebViewTransform.getElementGeometry(element);
            Dimension webViewDimension = WebViewTransform.getWebViewCSSSize(appium);

            switchToNativeContext(appium);

            Rectangle webViewRect = WebViewTransform.getWebViewNativeGeometry(appium);
            Point center = WebViewTransform.getCenter(webViewRect.intersection(
                    WebViewTransform.getElementNativeGeometry(webElementRect, webViewDimension, webViewRect)));

            appium.tap(1, center.getX(), center.getY(), TAP_DURATION_MS);

            switchToWebContext(appium);
        }
    }

}
