package tests;

import helper.BaseTestClass;
import org.junit.Ignore;
import org.junit.Test;
import pages.Common;
import pages.LeftPanel;
import pages.TopPanel;

// Class which contains set of smoke tests. It is just logical separation.
// It is meaningful to group tests to classes by their goals or business logic.
public class SmokeTests extends BaseTestClass {

    // @Test is standard JUnit notation to mark methods which perform tests
    @Test
    public void verifyLeftPanelContent() throws Exception {
        new TopPanel(driver, wait).clickMainMenuButton();

        LeftPanel leftPanel = new LeftPanel(driver, wait);
        leftPanel.verifyLinkIsVisible("Home");
        leftPanel.verifyLinkIsVisible("Random");
        leftPanel.verifyLinkIsVisible("Nearby");
        leftPanel.verifyLinkIsVisible("Watchlist");
        leftPanel.verifyLinkIsVisible("Settings");
        leftPanel.verifyLinkIsVisible("Log in");

        leftPanel.closeLeftPanel();
    }

    @Test
    public void searchForExistingArticle() throws Exception {
        new TopPanel(driver, wait).searchForText("Software test documentation");
        new Common(driver, wait).verifyPageHeader("Software test documentation");
    }

    //This class will be ignored and excluded from testing. To make this test runnable just remove @Ignore notation
    @Ignore
    @Test
    public void thisTestFails() throws Exception {
        new TopPanel(driver, wait).searchForText("Exhaustive testing");
        new Common(driver, wait).verifyPageHeader("Some wrong header");
    }

}
