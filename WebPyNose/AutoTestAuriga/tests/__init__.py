"""
Package 'tests' contains only test suites and test scenario definitions.
 Please, put service classes to separate sub-packages not to mix them with tests.
"""
