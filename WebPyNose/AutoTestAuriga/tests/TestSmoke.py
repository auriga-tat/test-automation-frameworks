# -*- coding: utf-8 -*-

from nose.tools import nottest

from AutoTestAuriga.helper.BaseTestClass import BaseTestClass
from AutoTestAuriga.pages.IndexPage import IndexPage
from AutoTestAuriga.pages.LeftPanel import LeftPanel
from AutoTestAuriga.pages.TopPanel import TopPanel
from AutoTestAuriga.pages.WikiPage import WikiPage


class TestSmoke(BaseTestClass):
    def test_01_verify_index_page_content(self):
        # Navigate ti Index page
        LeftPanel().click_index_link()
        # On Index page, verify header
        WikiPage().verify_page_header(u"Википедия:Алфавитный указатель")
        # Verify presence of some navigation elements on the Index page
        index_page = IndexPage()
        index_page.get_navigation_element_by_text(u"А")
        index_page.get_navigation_element_by_text(u"БА—БЯ")
        index_page.get_navigation_element_by_text(u"Ва")

    def test_02_search_for_existing_article(self):
        TopPanel().search_for_text(u"Тестирование программного обеспечения")
        WikiPage().verify_page_header(u"Тестирование программного обеспечения")

    # Remove or comment out the decorator @nottest to see how a test fails.
    @nottest
    def test_03_this_test_fails(self):
        WikiPage().verify_page_header(u"Такого заголовка быть не должно")
