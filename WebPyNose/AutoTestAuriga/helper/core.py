# -*- coding: utf-8 -*-

"""
The module defines some core functions of the framework,
like managing browser sessions and global settings.
"""

import os
from datetime import datetime
from selenium.webdriver import Firefox, FirefoxProfile, Chrome, ChromeOptions, Ie
from selenium.webdriver.support.ui import WebDriverWait
import logging

# define constants for the core
_DEFAULT_DRIVER_TYPE = "FF"
_ENVIRONMENT_DRIVER_TYPE = "test.browser"
_DEFAULT_SCREENSHOTS_FOLDER = "C:\\home\\screenshots"
_DEFAULT_EXPLICIT_WAIT_SEC = 30
_DEFAULT_IMPLICIT_WAIT_SEC = 10
_LOG_FORMAT = "%(asctime)s %(levelname)s (%(filename)s:%(lineno)d) %(message)s"


def get_logger(logger_name=__file__):
    """
    A factory method for creating unified loggers through the tests.
    """
    logger = logging.getLogger(logger_name)
    if not logger.handlers:
        # set format
        formatter = logging.Formatter(_LOG_FORMAT)
        # add a console logger
        console_handler = logging.StreamHandler()
        console_handler.setLevel(logging.DEBUG)
        console_handler.setFormatter(formatter)
        logger.addHandler(console_handler)
        # # to add logging to a file, uncomment following (and set the desired file path):
        # file_handler = logging.FileHandler("path\\to\\file.log.txt")
        # file_handler.setLevel(logging.DEBUG)
        # file_handler.setFormatter(formatter)
        # logger.addHandler(file_handler)
    return logger


# define protected variables for the core
_driver = None
_wait = None
_logger = get_logger()


def get_driver_type():
    """
    Returns required browser type (passed to test environment).
    In thing is passes, a default browser type is returned.
    See constants above to see defaults.
    """
    if os.environ.has_key(_ENVIRONMENT_DRIVER_TYPE):
        return os.environ[_ENVIRONMENT_DRIVER_TYPE]
    return _DEFAULT_DRIVER_TYPE


def get_current_driver():
    return _driver


def get_driver_wait():
    return _wait


def init_driver(driver_type=None):
    """
    Initializes the driver if it is not initialized yet.
    :param driver_type: FF, Chrome or IE
    :return: a tuple (driver, wait) containing a running driver and a default explicit wait object
    """
    global _driver, _wait

    # Create Browser instance
    if _driver is None:

        if driver_type is None:
            driver_type = get_driver_type()
        driver_type = driver_type.upper()

        if driver_type == "FF" or driver_type == "FIREFOX":
            _driver = init_firefox()
        elif driver_type == "CHROME" or driver_type == "GC":
            _driver = init_chrome()
        elif driver_type == "IE" or driver_type == "IEXPLORER" or driver_type == "INTERNETEXPLORER":
            _driver = init_ie()
        else:
            raise NotImplementedError("Browser type not supported: " + repr(driver_type))

    # Set up browser
    _driver.implicitly_wait(_DEFAULT_IMPLICIT_WAIT_SEC)
    _driver.maximize_window()

    # Create default WebDriverWait
    if _wait is None:
        _wait = WebDriverWait(_driver, _DEFAULT_EXPLICIT_WAIT_SEC)

    return _driver, _wait


def close_driver():
    """
    Closes the current driver session
    """
    global _driver, _wait
    if _driver is not None:
        _driver.quit()
    _driver = None
    _wait = None


def init_firefox():
    """
    Initializes a Firefox session and sets additional parameters if necessary.
    :return: An instance of Firefox WebDriver
    """
    firefox_profile = FirefoxProfile()
    capabilities = {}

    # # To specify a saved Firefox profile, use:
    # firefox_profile.profile_dir = "path\\to\\dir"

    # # To work via custom proxy, use:
    # firefox_profile.set_preference("network.proxy.type", 1)
    # firefox_profile.set_preference("network.proxy.http", "proxy.server.address")
    # firefox_profile.set_preference("network.proxy.http_port", "port_number")

    # # To add optional Firefox extensions, use:
    # firefox_profile.add_extension("path\\to\\extension.xpi")

    # # To download files without showing dialog. Download in silent mode without confirmation to the specified path.
    # firefox_profile.accept_untrusted_certs = True
    # firefox_profile.set_preference("browser.helperApps.alwaysAsk.force", False)
    # firefox_profile.set_preference("browser.download.manager.showWhenStarting", False)
    # firefox_profile.set_preference("browser.download.folderList", 2)
    # firefox_profile.set_preference("browser.download.dir", "C:\\temp")  # my downloading dir
    # firefox_profile.set_preference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", False)
    # firefox_profile.set_preference("browser.download.useDownloadDir", True)
    # firefox_profile.set_preference("browser.helperApps.neverAsk.saveToDisk", "application/pdf,text/html")
    # firefox_profile.set_preference("pdfjs.disabled", True)

    # # To allow access to WebDriver log from code, use:
    # capabilities["webdriver.log.driver"] = "ALL"

    # # To save WebDriver log to file, use:
    # capabilities["webdriver.log.file"] = "\\path\\to\\file.log.txt"

    firefox_profile.update_preferences()
    firefox = Firefox(firefox_profile=firefox_profile, capabilities=capabilities)
    return firefox


def init_chrome():
    """
    Initializes a Google Chrome / Chromium session and sets additional parameters if necessary.
    Expects the executable (chromedriver.exe) to be available on system path,
     else you should add the path explicitly as shown in comments.
    :return: An instance of Chrome WebDriver
    """
    chrome_options = ChromeOptions()
    capabilities = {}
    # # To specify an explicit path to ChromeDriver executable, use:
    # chrome_options.binary_location = "path\\to\\chromedriver.exe"
    chrome = Chrome(chrome_options=chrome_options, desired_capabilities=capabilities)
    return chrome


def init_ie():
    """
    Initializes an Internet Explorer session and sets additional parameters if necessary.
    Expects the executable (IEDriverServer.exe) to be available on system path,
     else you should add the path explicitly as shown in comments.
    :return: An instance of Ie WebDriver
    """
    ie = Ie()
    # # To specify an explicit path to IEDriverServer, use following INSTEAD of the parameterless Ie() constructor:
    # ie = Ie(executable_path='path\\to\\IEDriverServer.exe')
    # see also: https://code.google.com/p/selenium/wiki/InternetExplorerDriver#Required_Configuration
    return ie


def save_screenshot():
    """
    Takes screenshot from a current browser session and saves it as a PNG file.
    The screenshoths folder is created automatically if it does not exist,
    its default path is set above in _DEFAULT_SCREENSHOTS_FOLDER.
    """
    dest_folder = _DEFAULT_SCREENSHOTS_FOLDER
    file_path = os.path.join(
            dest_folder,
            datetime.now().strftime('%Y-%m-%d_%H-%M-%S') + '.png',
    )
    try:
        if not os.path.exists(dest_folder):
            os.makedirs(dest_folder)
        _driver.save_screenshot(file_path)
        _logger.error("Saved a screenshot to path: " + file_path)
    except:
        _logger.error("Could not save a screenshot to path: " + file_path)
        _logger.exception("Screenshot error")

