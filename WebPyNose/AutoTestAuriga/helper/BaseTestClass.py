# -*- coding: utf-8 -*-

import sys

from AutoTestAuriga.helper import core
from AutoTestAuriga.pages.Common import Common


class BaseTestClass(object):
    """
    A parent for all test classes, contains common initializations, setup and teardown.
    """

    def __init__(self):
        self.logger = core.get_logger(__file__)

    def setup(self):
        """
        Defines a setup hook that is executed before each test.
        For other types of hooks, see docs at:
        http://nose.readthedocs.org/en/latest/writing_tests.html
        """
        core.init_driver()
        Common().open_main_page()

    def teardown(self):
        """
        Defines a teardown hook that is executed after each test.
        For other types of hooks, see docs at:
        http://nose.readthedocs.org/en/latest/writing_tests.html
        """
        if sys.exc_info()[0]:
            core.save_screenshot()
        core.close_driver()
