# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

from AutoTestAuriga.pages.Common import Common


class LeftPanel(Common):
    """
    This page object contains methods that interact with the navigation on the left.
    """

    def get_index_link(self):
        return self.wait.until(EC.presence_of_element_located((By.LINK_TEXT, u"Указатель А — Я")))

    def click_index_link(self):
        self.logger.info("Clicking on Index link")
        self.get_index_link().click()
