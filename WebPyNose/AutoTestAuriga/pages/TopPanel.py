# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC

from AutoTestAuriga.pages.Common import Common


class TopPanel(Common):
    """
    This page object contains methods that interact with the site's top panel.
    """

    def get_search_input(self):
        return self.wait.until(EC.presence_of_element_located((By.XPATH, u"//input[@name='search']")))

    def search_for_text(self, text_for_searching):
        self.logger.info("Searching for text: " + text_for_searching)
        self.get_search_input().send_keys(text_for_searching + Keys.ENTER)
