package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

//Class which describes controls and actions related only to Left panel
public class LeftPanel extends Common{
    public static final Logger logger = Logger.getLogger(LeftPanel.class);

     public WebElement getIndexLink() {
         return wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Указатель А — Я")));
    }
}
