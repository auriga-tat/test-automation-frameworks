package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

//Class which describes controls and actions related only to Top panel
public class TopPanel extends Common{
    public static final Logger logger = Logger.getLogger(TopPanel.class);

     public WebElement getSearchInput() {
         return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='search']")));
    }
}
