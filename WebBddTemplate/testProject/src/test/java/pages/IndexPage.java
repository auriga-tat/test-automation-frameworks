package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

//Class which describes controls and actions related only to Top panel
public class IndexPage extends Common{
    public static final Logger logger = Logger.getLogger(IndexPage.class);

    public WebElement getNavigationElementByText(String textOfNavigationElement) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='" + textOfNavigationElement + "']")));
    }

}
