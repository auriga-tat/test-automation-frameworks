package helper;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by arkadiy.hachikyan on 12/2/2015.
 */@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        dryRun = false,
        features={"src/test/resources/features/"},
        glue={"helper", "stepDefinitions"},
        //glue={"src/test/java/emOffice/stepDefinitions/"},
        //tags = {"@smoke"},
        monochrome = true,
        /*        features={"src/test/resources/com/bdd/hpc.feature", "src/test/resources/com/bdd/smoke.feature"},
        glue={"com.bdd/HPCSteps", "com.bdd/SmokeSteps"},*/
        format = {"json:target/report.json",
                "pretty",
                "html:target/cucumber",
                "json:target/cucumber.json",
                "junit:target/cucumber-junit-report/allcukes.xml"
        }
)
public class MavenTestsRunnerForCucumber {
}
