package stepDefinitions;

import cucumber.api.java.en.And;
import junit.framework.Assert;
import org.apache.log4j.Logger;

/**
 * Created by arkadiy.hachikyan on 12/2/2015.
 */
public class Common extends pages.Common {
    public static final Logger logger = Logger.getLogger(Common.class);

    @And("^on any page I see that header is \"([^\"]*)\"$")
    public void onAnyPageISeeThatHeaderIs(String expectedPageTitle) throws Throwable {
        logger.info("Searching for header=" + expectedPageTitle);
        getHeaderElementByText(expectedPageTitle);
    }
}
