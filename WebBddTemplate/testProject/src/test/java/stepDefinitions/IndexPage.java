package stepDefinitions;

import cucumber.api.PendingException;
import cucumber.api.java.en.And;

/**
 * Created by arkadiy.hachikyan on 12/2/2015.
 */
public class IndexPage extends pages.IndexPage {

    @And("^on Index Page I see that navigation element \"([^\"]*)\" is presented$")
    public void onIndexPageISeeThatNavigationElementIsPresented(String textOfNavigationElement) throws Throwable {
        getNavigationElementByText(textOfNavigationElement);
    }

    @And("^this is a fake step$")
    public void thisIsAFakeStep() throws Throwable {
        throw new PendingException();
    }
}
