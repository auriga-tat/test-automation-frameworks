package stepDefinitions;

import cucumber.api.java.en.When;
import org.openqa.selenium.Keys;

/**
 * Created by arkadiy.hachikyan on 12/2/2015.
 */
public class TopPanel extends pages.TopPanel {

    @When("^on Top Panel I search for \"([^\"]*)\"$")
    public void onTopPanelISearchFor(String textForSearching) throws Throwable {
        logger.info("Searching for text = " + textForSearching);
        getSearchInput().sendKeys(textForSearching + Keys.ENTER);
    }
}
