package junitTestRunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features={"src/test/resources/feature"},
        glue={"helper", "stepDefinitions"},
        monochrome = true,
        strict = true,
        dryRun = false
       /* tags = {"@smoke"},
                format = {"json:target/report.json",
                "pretty",
                "html:target/cucumber",
                "json:target/cucumber.json",
                "junit:target/cucumber-junit-report/allcukes.xml"
        }*/
)
public class MavenRunnerForCucumberTest {
}
