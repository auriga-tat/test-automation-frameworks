import helper.Core;
import org.junit.Ignore;
import org.junit.Test;
import pages.Common;
import pages.IndexPage;
import pages.LeftPanel;
import pages.TopPanel;

//Class which contains set of smoke tests. It is just logical separation. it is meaningful to group tests by their goals or business logic
public class SmokeTests extends Core {

    //@Test is standard JUnit notation to mark methods which perform tests
    @Test
    public void verifyIndexPageContent() throws Exception {
        //Navigate to Index page
        LeftPanel leftPanel = new LeftPanel(driver, wait);
        leftPanel.clickIndexLink();

        //On any page verify header
        Common common = new Common(driver, wait);
        common.verifyPageHeader("Википедия:Алфавитный указатель");

        //Verify presence of some navigation elements on the Index page
        IndexPage indexPage = new IndexPage(driver, wait);
        indexPage.getNavigationElementByText("А");
        indexPage.getNavigationElementByText("БА—БЯ");
        indexPage.getNavigationElementByText("Ва");
    }


    @Test
    public void searchForExistingArticle() throws Exception {
        TopPanel topPanel = new TopPanel(driver, wait);
        topPanel.searchForText("Тестирование программного обеспечения");

        //On any page verify header
        Common common = new Common(driver, wait);
        common.verifyPageHeader("Тестирование программного обеспечения");
    }

    //This class will be ignored and excluded from testing. To make this test runnable just remove @Ignore notation
    @Ignore
    @Test
    public void thisTestFails() throws Exception {
        Common common = new Common(driver, wait);
        common.verifyPageHeader("some wrong header");
    }

}
