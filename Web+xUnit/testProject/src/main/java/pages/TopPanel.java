package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//Class which describes controls and actions related only to Top panel
public class TopPanel{
    public static final Logger logger = Logger.getLogger(TopPanel.class);
    private WebDriver driver;
    private WebDriverWait wait;

    public TopPanel(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    private WebElement getSearchInput() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//input[@name='search']")));
    }

    public void searchForText(String textForSearching) {
        logger.info("Searching for text = " + textForSearching);
        getSearchInput().sendKeys(textForSearching + Keys.ENTER);
    }
}
