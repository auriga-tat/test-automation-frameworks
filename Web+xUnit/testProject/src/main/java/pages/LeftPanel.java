package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


//Class which describes controls and actions related only to Left panel
public class LeftPanel {
    public static final Logger logger = Logger.getLogger(LeftPanel.class);
    private WebDriver driver;
    private WebDriverWait wait;

    public LeftPanel(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    private WebElement getIndexLink() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Указатель А — Я")));
    }

    public void clickIndexLink() {
        logger.info("Click by Index Link");
        getIndexLink().click();
    }
}
