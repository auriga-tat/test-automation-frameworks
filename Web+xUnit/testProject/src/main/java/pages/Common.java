package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//Class to perform common actions
//Common means not related to specific page and could be performed on any page
public class Common{
    public static final Logger logger = Logger.getLogger(Common.class);
    private WebDriver driver;
    private WebDriverWait wait;

    public Common(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }


    private WebElement getHeaderElement() {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[@id='firstHeading']")));
    }

    private WebElement getHeaderElementByText(String expectedPageTitle) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[@id='firstHeading' and text()='" + expectedPageTitle + "']")));
    }

    public void openMainPage()
    {
        logger.info("Opening Main page...");
        driver.get("https://ru.wikipedia.org");
    }

    public void verifyPageHeader(String expectedPageTitle)
    {
        getHeaderElementByText(expectedPageTitle);
        String currentPageHeader = getHeaderElement().getText();
        org.junit.Assert.assertEquals("Page header is incorrect", expectedPageTitle, currentPageHeader);
    }
}
