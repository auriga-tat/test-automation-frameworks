package pages;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

//Class which describes controls and actions related only to Top panel
public class IndexPage
{
    public static final Logger logger = Logger.getLogger(IndexPage.class);
    private WebDriver driver;
    private WebDriverWait wait;

    public IndexPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    public WebElement getNavigationElementByText(String textOfNavigationElement) {
        return wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='" + textOfNavigationElement + "']")));
    }

}
