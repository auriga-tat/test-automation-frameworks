package helper;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

//Class to describe actions to perform before and after each test. @Before and @After annotations are standard JUnit
public class Core {
    public WebDriver driver;
    public WebDriverWait wait;
    public static final Logger logger = Logger.getLogger(Core.class);
    public static final String startPageUrl = "https://ru.wikipedia.org";

    //Get browser name from the command line from "browser" parameter
    //Note that Firefox is default browser. Firefox will be used if you have not set any parameter (or have set invalid) in command line
    public static String driverName = System.getProperty("browser");

    @Before
    public void setUp() throws Exception {
        //Default browser is Firefox
        if (driverName == null) {
            driverName = "FF";
        }
        logger.info("Browser = " + driverName);

        if (driverName.equalsIgnoreCase("firefox") || driverName.equalsIgnoreCase("FF")) {
            FirefoxProfile profile = new FirefoxProfile();

            //Set preferences to not show dialog on downloading files. Download in silent mode without confirmation
//            profile.setAcceptUntrustedCertificates(true);
//            profile.setPreference("browser.helperApps.alwaysAsk.force", false);
//            profile.setPreference("browser.download.manager.showWhenStarting", false);
//            profile.setPreference("browser.download.folderList", 2);
//            profile.setPreference("browser.download.dir", "C:\\temp"); // my downloading dir
//            profile.setPreference("services.sync.prefs.sync.browser.download.manager.showWhenStarting", false);
//            profile.setPreference("browser.download.useDownloadDir", true);
//            profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/pdf,text/html");
//            profile.setPreference("pdfjs.disabled", true);

            // Run FireBug+FirePath if run tests in "debug" mode. Do not run FireBug+FirePath if in "run" mode
            if (java.lang.management.ManagementFactory.getRuntimeMXBean().
                    getInputArguments().toString().indexOf("-agentlib:jdwp") > 0) {
                profile.addExtension(ResourceReader.getInstance().getFileFromResource(
                        "fireFoxPlugins/firebug@software.joehewitt.com.xpi"));
                profile.setPreference("extensions.firebug.currentVersion", "9.9.9");
                // FirePath
                profile.addExtension(ResourceReader.getInstance().getFileFromResource(
                        "fireFoxPlugins/FireXPath@pierre.tholence.com.xpi"));
            }
            driver = new FirefoxDriver(profile);
        }
        else if (driverName.equalsIgnoreCase("chrome"))
        {
            System.setProperty("webdriver.chrome.driver", new File(
                    "src/main/resources/drivers/chrome/chromedriver.exe").getAbsolutePath());
            driver = new ChromeDriver();
        }
        else if (driverName.equalsIgnoreCase("IE") || driverName.equalsIgnoreCase("iexplorer") || driverName.equalsIgnoreCase("InternetExplorer") || driverName.equalsIgnoreCase("Internet Explorer"))
        {
            // You would need to set the protected mode settings to same for all zones in IE
            // See details here http://jimevansmusic.blogspot.in/2012/08/youre-doing-it-wrong-protected-mode-and.html
            String s = System.getProperty("os.arch");
            if (s.equals("x86")) {
                System.setProperty("webdriver.ie.driver", new File(
                        "src/main/resources/drivers/ie/x86/IEDriverServer.exe").getAbsolutePath());

            } else if (s.equals("amd64")) {
                System.setProperty("webdriver.ie.driver", new File(
                        "src/main/resources/drivers/ie/x64/IEDriverServer.exe").getAbsolutePath());

            } else {
                throw new Exception("Cannot set properly the IEDriverServer!");
            }
            driver = new InternetExplorerDriver();
        }
        else
        {
            throw new Exception("Browser is incorrect = " + driverName);
        }

        //Sets timeout for waiting for element state (present or visible). See details here http://docs.seleniumhq.org/docs/04_webdriver_advanced.jsp#explicit-and-implicit-waits
        wait = new WebDriverWait(driver, 30);
        logger.info("Opening " + startPageUrl);
        driver.get(startPageUrl);
    }

    @After
    public void tearDown() throws Exception {
        //Not closing browser here because actions on fail and success are override and also close the browser
        // driver.quit();
    }

    @Rule
    public TestWatcher watchman = new TestWatcher() {
        //Override actions on fail to make screenshots and do other actions
        @Override
        protected void failed(Throwable e, Description description) {
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                logger.error("Screenshot stored at:" + Paths.get(".").toAbsolutePath().normalize().toString() + "\\screenshots\\" + description.getMethodName() + ".png");
                FileUtils.copyFile(scrFile, new File(
                        "screenshots\\" + description.getMethodName() + ".png"));
            } catch (IOException e1) {
                logger.error("Fail to take screen shot");
            }
            logger.info("Test '" + description.getMethodName() + "' is Failed. Closing browser...");
            driver.quit();
        }

        //Override actions on success to only clg success and close browser
        @Override
        protected void succeeded(Description description) {
            logger.info("Test '" + description.getMethodName() + "' is Passed. Closing browser...");
            driver.quit();
        }
    };
}
