package steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helper.StepsBase;
import pages.BloodPressureEdit;

/**
 * Here we put a set of steps that are executed in Blood Pressure record editor
 */
public class BloodPressureEditorSteps extends StepsBase {
    @When("^in BP Editor I set systolic pressure to \"([^\"]*)\"$")
    public void inBPEditorISetSystolicPressureTo(String newValue) throws Throwable {
        new BloodPressureEdit(driver, wait).enterSystolicPressure(newValue);
    }

    @When("^in BP Editor I set diastolic pressure to \"([^\"]*)\"$")
    public void inBPEditorISetDiastolicPressureTo(String newValue) throws Throwable {
        new BloodPressureEdit(driver, wait).enterDiastolicPressure(newValue);
    }

    @When("^in BP Editor I set heart rate to \"([^\"]*)\"$")
    public void inBPEditorISetHeartRateTo(String newValue) throws Throwable {
        new BloodPressureEdit(driver, wait).enterHeartRate(newValue);
    }

    @When("^in BP Editor I add current record$")
    public void inBPEditorIAddCurrentRecord() throws Throwable {
        new BloodPressureEdit(driver, wait).clickAddReadingButton();
    }

    @Then("^in BP Editor I see that the last record value is \"([^\"]*)\"$")
    public void inBPEditorISeeThatTheLastRecordValueIsSys(String expectedValue) throws Throwable {
        String displayedValue = new BloodPressureEdit(driver, wait).getLastValue();
        org.junit.Assert.assertEquals("Last rcord value should be shown", expectedValue, displayedValue);
    }
}
