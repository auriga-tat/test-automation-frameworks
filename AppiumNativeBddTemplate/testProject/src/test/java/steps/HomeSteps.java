package steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import helper.StepsBase;
import pages.Home;

import java.util.List;

/**
 * Here we put a set of steps that are executed on Home screen
 */
public class HomeSteps extends StepsBase {

    @Given("^Home screen is opened$")
    public void homeScreenIsOpened() throws Throwable {

        // Go back up to 5 times until Home Screen activity appears
        Home home = new Home(driver, wait);
        for (int i = 0; i < 5; i++) {

            // Check current activity
            String currentActivity = home.getCurrentActivity();
            if (currentActivity.equals("com.michaelfester.heart.Heart")) {
                logger.info("Home screen is opened");
                return;
            }

            // If not yet at Home, try to go back
            logger.info("Current activity is " + currentActivity + ", going back to reach Home");
            driver.navigate().back();
            home.waitForChangingActivity(currentActivity, 7);
        }
        org.junit.Assert.fail("Home screen could not be reached");
    }

    @When("^on Home screen I press \"([^\"]*)\"$")
    public void onHomeScreenIPress(String iconLabel) throws Throwable {
        new Home(driver, wait).pressActionIconWithLabel(iconLabel);
    }

    @Then("^on Home screen I see following icons:$")
    public void onHomeScreenISeeFollowingIcons(List<String> expectedIconLabels) throws Throwable {
        for (String label : expectedIconLabels) {
            new Home(driver, wait).verifyActionIconWithLabelIsVisible(label);
        }
    }

}
