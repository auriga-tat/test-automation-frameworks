package steps;

import cucumber.api.java.en.When;
import helper.StepsBase;
import pages.ViewAdd;

/**
 * Here we put a set of steps that are executed on ViewAdd (Add New Record) screen
 */
public class ViewAddSteps extends StepsBase {
    @When("^on Add screen I start adding record \"([^\"]*)\"$")
    public void onAddScreenIStartAddingRecord(String recordType) throws Throwable {
        new ViewAdd(driver, wait).clickTextLabel(recordType);
    }

}
