package junitTestRunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        features={"src/test/resources/features"},
        glue = {"steps"},
        tags = {"@smoke"}, // or use -Dcucumber-options="--tags ..." instead
        plugin = {
                "json:target/report.json",
                "pretty",
                "html:target/cucumber",
                "json:target/cucumber.json",
                "junit:target/test-results-junit.xml"
        }
)
public class MavenRunnerForCucumberTest {
}
