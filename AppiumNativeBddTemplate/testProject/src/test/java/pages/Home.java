package pages;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.concurrent.TimeUnit;

/**
 * A page object to describe the application's home screen
 */
public class Home extends Common {
    public Home(AppiumDriver driver, WebDriverWait wait) {
        super(driver, wait);
    }

    /**
     * Waits for the initial dialog (Heart updates) to become visible
     *
     * @return The higher-level layout of the dialog if it is visible, else null.
     */
    private WebElement getInitialDialog() {
        try {
            return wait.withTimeout(10, TimeUnit.SECONDS)
                    .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
                            "//*[@resource-id='android:id/parentPanel' and .//android.widget.TextView[@text='Heart updates']]")));
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Waits for the "Close" button of the initial dialog (Heart updates)
     *
     * @return The button element
     */
    private WebElement getInitialDialogCloseButton() {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
                "//*[@resource-id='android:id/parentPanel']//android.widget.Button[@text='Close']")));
    }

    /**
     * Searches for an action icon by its label text
     */
    private WebElement getActionIconWithLabel(String labelText) {
        return wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(
                "//android.widget.TextView[@text='" + labelText + "']/../android.widget.ImageButton")));
    }

    /**
     * Closes initial dialog (supposed it is visible)
     */
    public void closeInitialDialog() {
        logger.info("Closing Heart Updates dialog");
        getInitialDialogCloseButton().click();
    }

    /**
     * Closes initial dialog if it has appeared
     */
    public void closeInitialDialogIfDisplayed() {
        if (getInitialDialog() != null) {
            closeInitialDialog();
        } else {
            logger.info("Heart Updates dialog is not shown");
        }
    }

    /**
     * Finds an action icon at the Home screen by the text of its label and clicks
     */
    public void pressActionIconWithLabel(String labelText) {
        logger.info("Pressing the Home screen button: " + labelText);
        getActionIconWithLabel(labelText).click();
    }

    /**
     * Finds an action icon exists and is visible for the label text provided
     */
    public void verifyActionIconWithLabelIsVisible(String labelText) {
        logger.info("Waiting for visibility of the Home screen button: " + labelText);
        try {
            getActionIconWithLabel(labelText);
        } catch (Exception e) {
            logger.error("Error searching for button: " + labelText, e);
            org.junit.Assert.fail("Button should be visible: " + labelText);
        }
    }

}
