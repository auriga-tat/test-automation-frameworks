package helper;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.ios.IOSDriver;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * A class that allows to create Appium sessions
 */
public class AppiumSessionFactory {

    private static final Logger logger = LogManager.getLogger(AppiumSessionFactory.class);

    public static AppiumDriver getAppiumDriver() throws IOException {

        // Set system properties "appium.address" and "appium.port" to specify where Appium is listening
        // The default values are 127.0.0.1:4723
        String appiumAddress = System.getProperty("appium.address");
        String appiumPort = System.getProperty("appium.port");

        String serverEndpoint = "http://" +
                (appiumAddress != null ? appiumAddress : "127.0.0.1") +
                ":" + (appiumPort != null ? appiumPort : "4723") + "/wd/hub";
        URL appiumServerUrl;
        try {
            appiumServerUrl = new URL(serverEndpoint);
        } catch (MalformedURLException e) {
            e.printStackTrace();
            throw new RuntimeException("Malformed Appium server address: " + serverEndpoint);
        }

        // Return the WebDriver depending on platform type
        DesiredCapabilities capabilities = getAppiumCapabilities();
        String platform = (String) capabilities.asMap().get("platformName");
        if (platform == null) {
            throw new IllegalArgumentException("platformName property not specified!");
        } else {
            platform = platform.toUpperCase();
            if (platform.equals("ANDROID")) {
                logger.info("Creating an Android driver connected to Appium at " + serverEndpoint);
                return new AndroidDriver(appiumServerUrl, capabilities);
            } else if (platform.equals("IOS")) {
                logger.info("Creating an iOS driver connected to Appium at " + serverEndpoint);
                return new IOSDriver(appiumServerUrl, capabilities);
            } else {
                throw new IllegalArgumentException("Unsupported mobile platform: " + platform);
            }
        }

    }

    /**
     * A private method to collect capabilities for Appium.
     * Capabilities are listed as properties in file <i>appium.properties</i>.
     * All properties may be overridden from command line.
     */
    private static DesiredCapabilities getAppiumCapabilities() throws IOException {
        // Convert path relative to target folder to absolute
        String workingDir = AppiumSessionFactory.class
                .getProtectionDomain().getCodeSource().getLocation().getPath();
        workingDir = StringUtils.removeStart(workingDir, "/");
        logger.info("Working directory is: " + workingDir);

        // Read file "appium.properties"
        Properties appiumProperties = new Properties();
        appiumProperties.load(new FileReader(Paths.get(workingDir, "appium.properties").toFile()));

        DesiredCapabilities capabilities = new DesiredCapabilities();
        for (Object key : appiumProperties.keySet()) {

            // Read capabilities from file, but override if equal system properties exist (with "appium." prefix)
            String capabilityName = (String) key;
            String capabilityValue = System.getProperty("appium." + capabilityName);
            if (capabilityValue == null) {
                capabilityValue = appiumProperties.getProperty(capabilityName);
            }

            if (!capabilityName.equals("app")) {
                capabilities.setCapability(capabilityName, capabilityValue);
            } else {
                // "app" is a special capability that sets a local path to an .apk
                // in properties, we set a relative path in the project resources

                capabilities.setCapability(capabilityName, Paths.get(workingDir, capabilityValue).toString());
            }
        }

        return capabilities;
    }

}
