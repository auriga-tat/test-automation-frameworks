package helper;

import io.appium.java_client.AppiumDriver;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;

/**
 * Class to describe common inherited procedures for Cucumber test steps.
 * It will be a parent class for all classes in 'steps' package. Try to keep it as simple as possible.
 */
public class StepsBase {
    protected static AppiumDriver driver;
    protected static WebDriverWait wait;
    protected static final Logger logger = LogManager.getLogger(StepsBase.class);

    /**
     * Initializes the Appium session to be used by all steps
     * Will be called from @Before hook.
     * @throws IOException
     */
    protected static void setUpDriver() throws IOException {
        // Initialize WebDriver for the desired browser
        driver = AppiumSessionFactory.getAppiumDriver();

        // Set timeout for waiting for element state (present or visible).
        // See details here http://docs.seleniumhq.org/docs/04_webdriver_advanced.jsp#explicit-and-implicit-waits
        wait = new WebDriverWait(driver, 30);
    }

    /**
     * Closes the active Appium session and cleans the working environment if necessary.
     */
    protected static void tearDownDriver() {
        // Add here any teardown activities, like cleaning application data
        driver.quit();
    }

}
