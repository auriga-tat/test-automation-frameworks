Feature: Smoke Tests
  As an automation tester
  I want to check the functionality of a BDD framework with Appium.

  Background:
  On Home screen I close the Heart update dialog

  @TC001
  @smoke
  Scenario: TC001. Checking elements of Home screen
    Given Home screen is opened
    Then on Home screen I see following icons:
      | Add      |
      | Journals |
      | Stats    |
      | Send     |
      | Prefs    |
      | About    |

  @TC002
  @smoke
  Scenario Outline: TC002. BP Editor. Last entered value should be shown
    Given Home screen is opened
    When on Home screen I press "Add"
    And on Add screen I start adding record "BP reading"
    And in BP Editor I set systolic pressure to "<Sys>"
    And in BP Editor I set diastolic pressure to "<Dia>"
    And in BP Editor I set heart rate to "<Rate>"
    And in BP Editor I add current record
    When on Add screen I start adding record "BP reading"
    Then in BP Editor I see that the last record value is "<Sys> | <Dia> | <Rate>"


    Examples:
    # 1st example should pass
    # 2nd should fail (it's not a correct negative test, just to demonstrate how the test fails)
      | Sys | Dia | Rate |
      | 130 | 90  | 75   |
      | A   | B   | C    |
